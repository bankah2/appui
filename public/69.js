(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[69],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/apps/wordpress-posts/index.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/apps/wordpress-posts/index.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _kit_widgets_Lists_15_index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/@kit/widgets/Lists/15/index */ "./resources/js/@kit/widgets/Lists/15/index.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    KitList15: _kit_widgets_Lists_15_index__WEBPACK_IMPORTED_MODULE_0__["default"]
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/apps/wordpress-posts/index.vue?vue&type=template&id=2418ad24&":
/*!************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/apps/wordpress-posts/index.vue?vue&type=template&id=2418ad24& ***!
  \************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "row" }, [
      _c(
        "div",
        { staticClass: "col-xl-9 col-lg-12" },
        [
          _vm._m(0),
          _vm._v(" "),
          _vm._m(1),
          _vm._v(" "),
          _vm._m(2),
          _vm._v(" "),
          _c("a-pagination", { attrs: { total: 50 } })
        ],
        1
      ),
      _vm._v(" "),
      _c("div", { staticClass: "col-xl-3 col-lg-12" }, [
        _vm._m(3),
        _vm._v(" "),
        _vm._m(4),
        _vm._v(" "),
        _vm._m(5),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "pb-4 mb-3 border-bottom" },
          [
            _c("div", { staticClass: "font-weight-bold mb-3" }, [
              _vm._v("Latest Posts")
            ]),
            _vm._v(" "),
            _c("kit-list-15")
          ],
          1
        )
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "card" }, [
      _c("div", { staticClass: "card-body" }, [
        _c("div", { staticClass: "mb-2" }, [
          _c(
            "a",
            {
              staticClass: "text-dark font-size-24 font-weight-bold",
              attrs: { href: "javascript: void(0);" }
            },
            [
              _vm._v(
                "[Feature Request] How to enable custom font that comes from svg #2460"
              )
            ]
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "mb-3" }, [
          _c(
            "a",
            {
              staticClass: "font-weight-bold",
              attrs: { href: "javascript: void(0);" }
            },
            [_vm._v("zxs2162")]
          ),
          _vm._v(
            " wrote this post 12\n            days ago · 0 comments\n          "
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "mb-4" }, [
          _c(
            "a",
            {
              staticClass:
                "badge text-blue text-uppercase bg-light font-size-12 mr-2",
              attrs: { href: "javascript: void(0);" }
            },
            [_vm._v("Umi")]
          ),
          _vm._v(" "),
          _c(
            "a",
            {
              staticClass:
                "badge text-blue text-uppercase bg-light font-size-12 mr-2",
              attrs: { href: "javascript: void(0);" }
            },
            [_vm._v("React-framework")]
          ),
          _vm._v(" "),
          _c(
            "a",
            {
              staticClass:
                "badge text-blue text-uppercase bg-light font-size-12 mr-2",
              attrs: { href: "javascript: void(0);" }
            },
            [_vm._v("Umijs")]
          )
        ]),
        _vm._v(" "),
        _c("div", [
          _c("p", [
            _vm._v(
              "\n              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil laborum est perferendis\n              consectetur corporis esse labore minima molestias, exercitationem consequuntur! Lorem\n              ipsum dolor sit amet, consectetur adipisicing elit. Nihil laborum est perferendis\n              consectetur corporis esse labore minima molestias, exercitationem consequuntur! Lorem\n              ipsum dolor sit amet, consectetur adipisicing elit. Nihil laborum est perferendis\n              consectetur corporis esse labore minima molestias, exercitationem consequuntur! Lorem\n              ipsum dolor sit amet, consectetur adipisicing elit. Nihil laborum est perferendis\n              consectetur corporis esse labore minima molestias, exercitationem consequuntur!\n            "
            )
          ]),
          _vm._v(" "),
          _c("p", [
            _vm._v(
              "\n              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil laborum est perferendis\n              consectetur corporis esse labore minima molestias, exercitationem consequuntur! Lorem\n              ipsum dolor sit amet, consectetur adipisicing elit. Nihil laborum est perferendis\n              consectetur corporis esse labore minima molestias, exercitationem consequuntur!\n            "
            )
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "card" }, [
      _c("div", { staticClass: "card-body" }, [
        _c("div", { staticClass: "mb-2" }, [
          _c(
            "a",
            {
              staticClass: "text-dark font-size-24 font-weight-bold",
              attrs: { href: "javascript: void(0);" }
            },
            [
              _vm._v(
                "London's mayor compared President Trump to an 11-year-old child"
              )
            ]
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "mb-3" }, [
          _c(
            "a",
            {
              staticClass: "font-weight-bold",
              attrs: { href: "javascript: void(0);" }
            },
            [_vm._v("hepu")]
          ),
          _vm._v(
            " wrote this post 12 days\n            ago · 0 comments\n          "
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "mb-4" }, [
          _c(
            "a",
            {
              staticClass:
                "badge text-blue text-uppercase bg-light font-size-12 mr-2",
              attrs: { href: "javascript: void(0);" }
            },
            [_vm._v("Umi")]
          ),
          _vm._v(" "),
          _c(
            "a",
            {
              staticClass:
                "badge text-blue text-uppercase bg-light font-size-12 mr-2",
              attrs: { href: "javascript: void(0);" }
            },
            [_vm._v("React-framework")]
          ),
          _vm._v(" "),
          _c(
            "a",
            {
              staticClass:
                "badge text-blue text-uppercase bg-light font-size-12 mr-2",
              attrs: { href: "javascript: void(0);" }
            },
            [_vm._v("Umijs")]
          )
        ]),
        _vm._v(" "),
        _c("div", [
          _c("p", [
            _vm._v(
              "\n              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil laborum est perferendis\n              consectetur corporis esse labore minima molestias, exercitationem consequuntur! Lorem\n              ipsum dolor sit amet, consectetur adipisicing elit. Nihil laborum est perferendis\n              consectetur corporis esse labore minima molestias, exercitationem consequuntur! Lorem\n              ipsum dolor sit amet, consectetur adipisicing elit. Nihil laborum est perferendis\n              consectetur corporis esse labore minima molestias, exercitationem consequuntur! Lorem\n              ipsum dolor sit amet, consectetur adipisicing elit. Nihil laborum est perferendis\n              consectetur corporis esse labore minima molestias, exercitationem consequuntur!\n            "
            )
          ]),
          _vm._v(" "),
          _c("p", [
            _vm._v(
              "\n              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil laborum est perferendis\n              consectetur corporis esse labore minima molestias, exercitationem consequuntur! Lorem\n              ipsum dolor sit amet, consectetur adipisicing elit. Nihil laborum est perferendis\n              consectetur corporis esse labore minima molestias, exercitationem consequuntur!\n            "
            )
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "card" }, [
      _c("div", { staticClass: "card-body" }, [
        _c("div", { staticClass: "mb-2" }, [
          _c(
            "a",
            {
              staticClass: "text-dark font-size-24 font-weight-bold",
              attrs: { href: "javascript: void(0);" }
            },
            [_vm._v("What D-Day taught my grandpa")]
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "mb-3" }, [
          _c(
            "a",
            {
              staticClass: "font-weight-bold",
              attrs: { href: "javascript: void(0);" }
            },
            [_vm._v("Dankin")]
          ),
          _vm._v(
            " wrote this post 12 days\n            ago · 0 comments\n          "
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "mb-4" }, [
          _c(
            "a",
            {
              staticClass:
                "badge text-blue text-uppercase bg-light font-size-12 mr-2",
              attrs: { href: "javascript: void(0);" }
            },
            [_vm._v("Umi")]
          ),
          _vm._v(" "),
          _c(
            "a",
            {
              staticClass:
                "badge text-blue text-uppercase bg-light font-size-12 mr-2",
              attrs: { href: "javascript: void(0);" }
            },
            [_vm._v("React-framework")]
          ),
          _vm._v(" "),
          _c(
            "a",
            {
              staticClass:
                "badge text-blue text-uppercase bg-light font-size-12 mr-2",
              attrs: { href: "javascript: void(0);" }
            },
            [_vm._v("Umijs")]
          )
        ]),
        _vm._v(" "),
        _c("div", [
          _c("p", [
            _vm._v(
              "\n              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil laborum est perferendis\n              consectetur corporis esse labore minima molestias, exercitationem consequuntur! Lorem\n              ipsum dolor sit amet, consectetur adipisicing elit. Nihil laborum est perferendis\n              consectetur corporis esse labore minima molestias, exercitationem consequuntur! Lorem\n              ipsum dolor sit amet, consectetur adipisicing elit. Nihil laborum est perferendis\n              consectetur corporis esse labore minima molestias, exercitationem consequuntur! Lorem\n              ipsum dolor sit amet, consectetur adipisicing elit. Nihil laborum est perferendis\n              consectetur corporis esse labore minima molestias, exercitationem consequuntur!\n            "
            )
          ]),
          _vm._v(" "),
          _c("p", [
            _vm._v(
              "\n              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil laborum est perferendis\n              consectetur corporis esse labore minima molestias, exercitationem consequuntur! Lorem\n              ipsum dolor sit amet, consectetur adipisicing elit. Nihil laborum est perferendis\n              consectetur corporis esse labore minima molestias, exercitationem consequuntur!\n            "
            )
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "pb-4 mb-3 border-bottom" }, [
      _c(
        "label",
        {
          staticClass: "font-weight-bold d-block",
          attrs: { for: "search-input" }
        },
        [
          _c("span", { staticClass: "mb-2 d-inline-block" }, [
            _vm._v("Search Post")
          ]),
          _vm._v(" "),
          _c("input", {
            staticClass: "form-control width-100p",
            attrs: {
              type: "text",
              placeholder: "Search post...",
              id: "search-input"
            }
          })
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "pb-4 mb-3 border-bottom" }, [
      _c(
        "label",
        {
          staticClass: "font-weight-bold d-block",
          attrs: { for: "subscribe-input" }
        },
        [
          _c("span", { staticClass: "mb-2 d-inline-block" }, [
            _vm._v("Subscribe")
          ]),
          _vm._v(" "),
          _c("input", {
            staticClass: "form-control width-100p",
            attrs: {
              type: "text",
              id: "subscribe-input",
              placeholder: "Enter your email..."
            }
          })
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "pb-4 mb-3 border-bottom" }, [
      _c("div", { staticClass: "font-weight-bold mb-2" }, [
        _vm._v("Categories")
      ]),
      _vm._v(" "),
      _c("div", [
        _c(
          "a",
          {
            staticClass:
              "badge text-blue text-uppercase bg-light font-size-12 mr-2",
            attrs: { href: "javascript: void(0);" }
          },
          [_vm._v("Umi")]
        ),
        _vm._v(" "),
        _c(
          "a",
          {
            staticClass:
              "badge text-blue text-uppercase bg-light font-size-12 mr-2",
            attrs: { href: "javascript: void(0);" }
          },
          [_vm._v("React-framework")]
        ),
        _vm._v(" "),
        _c(
          "a",
          {
            staticClass:
              "badge text-blue text-uppercase bg-light font-size-12 mr-2",
            attrs: { href: "javascript: void(0);" }
          },
          [_vm._v("Umijs")]
        )
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/apps/wordpress-posts/index.vue":
/*!***********************************************************!*\
  !*** ./resources/js/views/apps/wordpress-posts/index.vue ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _index_vue_vue_type_template_id_2418ad24___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.vue?vue&type=template&id=2418ad24& */ "./resources/js/views/apps/wordpress-posts/index.vue?vue&type=template&id=2418ad24&");
/* harmony import */ var _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.vue?vue&type=script&lang=js& */ "./resources/js/views/apps/wordpress-posts/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _index_vue_vue_type_template_id_2418ad24___WEBPACK_IMPORTED_MODULE_0__["render"],
  _index_vue_vue_type_template_id_2418ad24___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/apps/wordpress-posts/index.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/apps/wordpress-posts/index.vue?vue&type=script&lang=js&":
/*!************************************************************************************!*\
  !*** ./resources/js/views/apps/wordpress-posts/index.vue?vue&type=script&lang=js& ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/apps/wordpress-posts/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/apps/wordpress-posts/index.vue?vue&type=template&id=2418ad24&":
/*!******************************************************************************************!*\
  !*** ./resources/js/views/apps/wordpress-posts/index.vue?vue&type=template&id=2418ad24& ***!
  \******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_2418ad24___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=template&id=2418ad24& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/apps/wordpress-posts/index.vue?vue&type=template&id=2418ad24&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_2418ad24___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_2418ad24___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);