(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[38],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/@kit/widgets/General/16/index.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/@kit/widgets/General/16/index.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'KitGeneral16',
  props: ['isNew', 'isFavorite', 'image', 'name', 'price', 'oldPrice'],
  data: function data() {
    return {
      favorite: this.isFavorite
    };
  },
  methods: {
    toggleFavorite: function toggleFavorite(event) {
      event.preventDefault();
      this.favorite = !this.favorite;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ecommerce/product-catalog/index.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/ecommerce/product-catalog/index.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _kit_widgets_General_16_index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/@kit/widgets/General/16/index */ "./resources/js/@kit/widgets/General/16/index.vue");
/* harmony import */ var _data_json__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./data.json */ "./resources/js/views/ecommerce/product-catalog/data.json");
var _data_json__WEBPACK_IMPORTED_MODULE_1___namespace = /*#__PURE__*/__webpack_require__.t(/*! ./data.json */ "./resources/js/views/ecommerce/product-catalog/data.json", 1);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    AirGeneral16: _kit_widgets_General_16_index__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {
      products: _data_json__WEBPACK_IMPORTED_MODULE_1__
    };
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/@kit/widgets/General/16/index.vue?vue&type=style&index=0&lang=scss&module=true&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-2!./node_modules/sass-loader/dist/cjs.js??ref--8-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/@kit/widgets/General/16/index.vue?vue&type=style&index=0&lang=scss&module=true& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".new {\n  position: absolute;\n  width: 100px;\n  height: 100px;\n  background-color: #f00;\n  text-align: center;\n  color: #fff;\n  top: -50px;\n  right: -50px;\n  line-height: 163px;\n  transform: rotate(45deg);\n  text-transform: uppercase;\n}\n.favorite {\n  position: absolute;\n  top: 1.33rem;\n  left: 1.66rem;\n  color: #dde2ec;\n}\n.favorite:focus {\n  color: #dde2ec;\n}\n.favorite:focus:hover {\n  color: #dde2ec;\n}\n.thumb {\n  display: flex;\n  align-items: center;\n  border-radius: 5px;\n  overflow: hidden;\n}\n.thumb img {\n  margin: auto;\n  max-height: 100%;\n  width: auto;\n}\n.image {\n  display: flex;\n  align-items: center;\n}\n.image img {\n  margin: auto;\n  max-height: 100%;\n  width: auto;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/@kit/widgets/General/16/index.vue?vue&type=style&index=0&lang=scss&module=true&":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-2!./node_modules/sass-loader/dist/cjs.js??ref--8-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/@kit/widgets/General/16/index.vue?vue&type=style&index=0&lang=scss&module=true& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/src??ref--8-2!../../../../../../node_modules/sass-loader/dist/cjs.js??ref--8-3!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=style&index=0&lang=scss&module=true& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/@kit/widgets/General/16/index.vue?vue&type=style&index=0&lang=scss&module=true&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/@kit/widgets/General/16/index.vue?vue&type=template&id=90353e0c&":
/*!*********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/@kit/widgets/General/16/index.vue?vue&type=template&id=90353e0c& ***!
  \*********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "card overflow-hidden" }, [
    _vm.isNew
      ? _c("div", { class: _vm.$style.new }, [_vm._v("\n    New\n  ")])
      : _vm._e(),
    _vm._v(" "),
    _c("div", { staticClass: "card-body" }, [
      _c(
        "a",
        {
          class: [
            _vm.$style.favorite,
            _vm.favorite ? "text-dark" : "text-gray-3"
          ],
          attrs: { href: "#" },
          on: { click: _vm.toggleFavorite }
        },
        [_c("i", { staticClass: "fe fe-heart font-size-21" })]
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "border-bottom height-250 mb-3",
          class: _vm.$style.image
        },
        [
          _c("img", {
            staticClass: "img-fluid",
            attrs: { src: _vm.image, alt: _vm.name }
          })
        ]
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "font-size-24 font-weight-bold text-dark mb-2" },
        [
          _vm._v("\n      " + _vm._s(_vm.price) + "\n      "),
          _vm.oldPrice
            ? _c("del", { staticClass: "align-text-top font-size-14 mr-2" }, [
                _vm._v("\n        " + _vm._s(_vm.oldPrice) + "\n      ")
              ])
            : _vm._e()
        ]
      ),
      _vm._v(" "),
      _c("div", [
        _c(
          "a",
          {
            staticClass: "text-blue font-size-18",
            attrs: { href: "javascript:void(0);" }
          },
          [_vm._v("\n        " + _vm._s(_vm.name) + "\n      ")]
        )
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ecommerce/product-catalog/index.vue?vue&type=template&id=352f1240&":
/*!*****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/ecommerce/product-catalog/index.vue?vue&type=template&id=352f1240& ***!
  \*****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _vm._m(0),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "row" },
      _vm._l(_vm.products, function(product, index) {
        return _c(
          "div",
          { key: index, staticClass: "col-xl-4 col-lg-6" },
          [
            _c("air-general-16", {
              attrs: {
                isNew: product.isNew,
                isFavorite: product.isFavorite,
                image: product.image,
                name: product.name,
                price: product.price,
                oldPrice: product.oldPrice
              }
            })
          ],
          1
        )
      }),
      0
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "air__utils__heading" }, [
      _c("strong", [_vm._v("Ecommerce: Product Catalog")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/@kit/widgets/General/16/index.vue":
/*!********************************************************!*\
  !*** ./resources/js/@kit/widgets/General/16/index.vue ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var _index_vue_vue_type_template_id_90353e0c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.vue?vue&type=template&id=90353e0c& */ "./resources/js/@kit/widgets/General/16/index.vue?vue&type=template&id=90353e0c&");
/* harmony import */ var _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.vue?vue&type=script&lang=js& */ "./resources/js/@kit/widgets/General/16/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _index_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./index.vue?vue&type=style&index=0&lang=scss&module=true& */ "./resources/js/@kit/widgets/General/16/index.vue?vue&type=style&index=0&lang=scss&module=true&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





var cssModules = {}
var disposed = false

function injectStyles (context) {
  if (disposed) return
  
        cssModules["$style"] = (_index_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_2__["default"].locals || _index_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_2__["default"])
        Object.defineProperty(this, "$style", {
          configurable: true,
          get: function () {
            return cssModules["$style"]
          }
        })
      
}


  module.hot && false



        module.hot && false

/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _index_vue_vue_type_template_id_90353e0c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _index_vue_vue_type_template_id_90353e0c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  injectStyles,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/@kit/widgets/General/16/index.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./resources/js/@kit/widgets/General/16/index.vue?vue&type=script&lang=js&":
/*!*********************************************************************************!*\
  !*** ./resources/js/@kit/widgets/General/16/index.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/@kit/widgets/General/16/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/@kit/widgets/General/16/index.vue?vue&type=style&index=0&lang=scss&module=true&":
/*!******************************************************************************************************!*\
  !*** ./resources/js/@kit/widgets/General/16/index.vue?vue&type=style&index=0&lang=scss&module=true& ***!
  \******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/style-loader!../../../../../../node_modules/css-loader!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/src??ref--8-2!../../../../../../node_modules/sass-loader/dist/cjs.js??ref--8-3!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=style&index=0&lang=scss&module=true& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/@kit/widgets/General/16/index.vue?vue&type=style&index=0&lang=scss&module=true&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (default from non-harmony) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_0___default.a; });
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 

/***/ }),

/***/ "./resources/js/@kit/widgets/General/16/index.vue?vue&type=template&id=90353e0c&":
/*!***************************************************************************************!*\
  !*** ./resources/js/@kit/widgets/General/16/index.vue?vue&type=template&id=90353e0c& ***!
  \***************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_90353e0c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=template&id=90353e0c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/@kit/widgets/General/16/index.vue?vue&type=template&id=90353e0c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_90353e0c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_90353e0c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/ecommerce/product-catalog/data.json":
/*!****************************************************************!*\
  !*** ./resources/js/views/ecommerce/product-catalog/data.json ***!
  \****************************************************************/
/*! exports provided: 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, default */
/***/ (function(module) {

module.exports = JSON.parse("[{\"isNew\":true,\"isFavorite\":false,\"image\":\"resources/images/products/001.jpg\",\"name\":\"TP-Link AC1750 Smart WiFi Router - Dual Band Gigabit Wireless Internet Routers for Home\",\"price\":\"199.28\",\"oldPrice\":\"299.28\"},{\"isNew\":false,\"isFavorite\":true,\"image\":\"resources/images/products/002.jpg\",\"name\":\"TP-Link AC1750 Smart WiFi Router - Dual Band Gigabit Wireless Internet Routers for Home\",\"price\":\"99.99\",\"oldPrice\":\"199.99\"},{\"isNew\":false,\"isFavorite\":false,\"image\":\"resources/images/products/003.jpg\",\"name\":\"TP-Link AC1750 Smart WiFi Router - Dual Band Gigabit Wireless Internet Routers for Home\",\"price\":\"245.21\",\"oldPrice\":\"419.58\"},{\"isNew\":true,\"isFavorite\":true,\"image\":\"resources/images/products/004.jpg\",\"name\":\"TP-Link AC1750 Smart WiFi Router - Dual Band Gigabit Wireless Internet Routers for Home\",\"price\":\"199.99\",\"oldPrice\":\"499.99\"},{\"isNew\":true,\"isFavorite\":false,\"image\":\"resources/images/products/001.jpg\",\"name\":\"TP-Link AC1750 Smart WiFi Router - Dual Band Gigabit Wireless Internet Routers for Home\",\"price\":\"199.28\",\"oldPrice\":\"299.28\"},{\"isNew\":false,\"isFavorite\":true,\"image\":\"resources/images/products/002.jpg\",\"name\":\"TP-Link AC1750 Smart WiFi Router - Dual Band Gigabit Wireless Internet Routers for Home\",\"price\":\"99.99\",\"oldPrice\":\"199.99\"},{\"isNew\":false,\"isFavorite\":false,\"image\":\"resources/images/products/003.jpg\",\"name\":\"TP-Link AC1750 Smart WiFi Router - Dual Band Gigabit Wireless Internet Routers for Home\",\"price\":\"245.21\",\"oldPrice\":\"419.58\"},{\"isNew\":true,\"isFavorite\":true,\"image\":\"resources/images/products/004.jpg\",\"name\":\"TP-Link AC1750 Smart WiFi Router - Dual Band Gigabit Wireless Internet Routers for Home\",\"price\":\"199.99\",\"oldPrice\":\"499.99\"},{\"isNew\":true,\"isFavorite\":false,\"image\":\"resources/images/products/001.jpg\",\"name\":\"TP-Link AC1750 Smart WiFi Router - Dual Band Gigabit Wireless Internet Routers for Home\",\"price\":\"199.28\",\"oldPrice\":\"299.28\"},{\"isNew\":false,\"isFavorite\":true,\"image\":\"resources/images/products/002.jpg\",\"name\":\"TP-Link AC1750 Smart WiFi Router - Dual Band Gigabit Wireless Internet Routers for Home\",\"price\":\"99.99\",\"oldPrice\":\"199.99\"},{\"isNew\":false,\"isFavorite\":false,\"image\":\"resources/images/products/003.jpg\",\"name\":\"TP-Link AC1750 Smart WiFi Router - Dual Band Gigabit Wireless Internet Routers for Home\",\"price\":\"245.21\",\"oldPrice\":\"419.58\"},{\"isNew\":true,\"isFavorite\":true,\"image\":\"resources/images/products/004.jpg\",\"name\":\"TP-Link AC1750 Smart WiFi Router - Dual Band Gigabit Wireless Internet Routers for Home\",\"price\":\"199.99\",\"oldPrice\":\"499.99\"}]");

/***/ }),

/***/ "./resources/js/views/ecommerce/product-catalog/index.vue":
/*!****************************************************************!*\
  !*** ./resources/js/views/ecommerce/product-catalog/index.vue ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _index_vue_vue_type_template_id_352f1240___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.vue?vue&type=template&id=352f1240& */ "./resources/js/views/ecommerce/product-catalog/index.vue?vue&type=template&id=352f1240&");
/* harmony import */ var _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.vue?vue&type=script&lang=js& */ "./resources/js/views/ecommerce/product-catalog/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _index_vue_vue_type_template_id_352f1240___WEBPACK_IMPORTED_MODULE_0__["render"],
  _index_vue_vue_type_template_id_352f1240___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/ecommerce/product-catalog/index.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/ecommerce/product-catalog/index.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/views/ecommerce/product-catalog/index.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ecommerce/product-catalog/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/ecommerce/product-catalog/index.vue?vue&type=template&id=352f1240&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/views/ecommerce/product-catalog/index.vue?vue&type=template&id=352f1240& ***!
  \***********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_352f1240___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=template&id=352f1240& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ecommerce/product-catalog/index.vue?vue&type=template&id=352f1240&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_352f1240___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_352f1240___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);