(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[33],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/@kit/widgets/Charts/3/index.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/@kit/widgets/Charts/3/index.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _data_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./data.json */ "./resources/js/@kit/widgets/Charts/3/data.json");
var _data_json__WEBPACK_IMPORTED_MODULE_0___namespace = /*#__PURE__*/__webpack_require__.t(/*! ./data.json */ "./resources/js/@kit/widgets/Charts/3/data.json", 1);
/* harmony import */ var v_chartist__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! v-chartist */ "./node_modules/v-chartist/dist/vue-chartist.min.js");
/* harmony import */ var v_chartist__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(v_chartist__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var chartist_plugin_tooltips_updated__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! chartist-plugin-tooltips-updated */ "./node_modules/chartist-plugin-tooltips-updated/dist/chartist-plugin-tooltip.js");
/* harmony import */ var chartist_plugin_tooltips_updated__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(chartist_plugin_tooltips_updated__WEBPACK_IMPORTED_MODULE_2__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'KitChart3',
  components: {
    'vue-chartist': v_chartist__WEBPACK_IMPORTED_MODULE_1___default.a
  },
  data: function data() {
    var options = {
      low: 0,
      chartPadding: {
        right: 0,
        left: 0,
        top: 5,
        bottom: 0
      },
      fullWidth: true,
      showPoint: true,
      lineSmooth: false,
      axisY: {
        showGrid: false,
        showLabel: false,
        offset: 0
      },
      axisX: {
        showGrid: false,
        showLabel: false,
        offset: 0
      },
      showArea: true,
      plugins: [chartist_plugin_tooltips_updated__WEBPACK_IMPORTED_MODULE_2___default()({
        anchorToPoint: false,
        appendToBody: true,
        seriesName: false
      })]
    };
    var listener = {
      draw: function draw(item) {
        if (item.type === 'bar') {
          item.group.elem('line', {
            x1: item.x1,
            x2: item.x2,
            y1: item.y2,
            y2: 0,
            stroke: '#e4e9f0',
            'stroke-width': '10'
          });
        }
      }
    };
    return {
      data: _data_json__WEBPACK_IMPORTED_MODULE_0__,
      options: options,
      listener: listener
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/@kit/widgets/Lists/11/index.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/@kit/widgets/Lists/11/index.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'KitList11'
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ecommerce/dashboard/index.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/ecommerce/dashboard/index.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _kit_widgets_Charts_3_index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/@kit/widgets/Charts/3/index */ "./resources/js/@kit/widgets/Charts/3/index.vue");
/* harmony import */ var _kit_widgets_General_2_index__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/@kit/widgets/General/2/index */ "./resources/js/@kit/widgets/General/2/index.vue");
/* harmony import */ var _kit_widgets_General_2v1_index__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/@kit/widgets/General/2v1/index */ "./resources/js/@kit/widgets/General/2v1/index.vue");
/* harmony import */ var _kit_widgets_General_2v2_index__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/@kit/widgets/General/2v2/index */ "./resources/js/@kit/widgets/General/2v2/index.vue");
/* harmony import */ var _kit_widgets_General_2v3_index__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/@kit/widgets/General/2v3/index */ "./resources/js/@kit/widgets/General/2v3/index.vue");
/* harmony import */ var _kit_widgets_General_16_index__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @/@kit/widgets/General/16/index */ "./resources/js/@kit/widgets/General/16/index.vue");
/* harmony import */ var _kit_widgets_Lists_11_index__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @/@kit/widgets/Lists/11/index */ "./resources/js/@kit/widgets/Lists/11/index.vue");
/* harmony import */ var _kit_widgets_Lists_12_index__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @/@kit/widgets/Lists/12/index */ "./resources/js/@kit/widgets/Lists/12/index.vue");
/* harmony import */ var _data_json__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./data.json */ "./resources/js/views/ecommerce/dashboard/data.json");
var _data_json__WEBPACK_IMPORTED_MODULE_8___namespace = /*#__PURE__*/__webpack_require__.t(/*! ./data.json */ "./resources/js/views/ecommerce/dashboard/data.json", 1);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//









/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    AirChart3: _kit_widgets_Charts_3_index__WEBPACK_IMPORTED_MODULE_0__["default"],
    AirGeneral2: _kit_widgets_General_2_index__WEBPACK_IMPORTED_MODULE_1__["default"],
    AirGeneral2v1: _kit_widgets_General_2v1_index__WEBPACK_IMPORTED_MODULE_2__["default"],
    AirGeneral2v2: _kit_widgets_General_2v2_index__WEBPACK_IMPORTED_MODULE_3__["default"],
    AirGeneral2v3: _kit_widgets_General_2v3_index__WEBPACK_IMPORTED_MODULE_4__["default"],
    AirGeneral16: _kit_widgets_General_16_index__WEBPACK_IMPORTED_MODULE_5__["default"],
    AirList11: _kit_widgets_Lists_11_index__WEBPACK_IMPORTED_MODULE_6__["default"],
    AirList12: _kit_widgets_Lists_12_index__WEBPACK_IMPORTED_MODULE_7__["default"]
  },
  data: function data() {
    return {
      products: _data_json__WEBPACK_IMPORTED_MODULE_8__
    };
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/@kit/widgets/Charts/3/index.vue?vue&type=style&index=0&lang=scss&module=true&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-2!./node_modules/sass-loader/dist/cjs.js??ref--8-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/@kit/widgets/Charts/3/index.vue?vue&type=style&index=0&lang=scss&module=true& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".donut {\n  display: inline-block;\n  width: 1.06rem;\n  height: 1.06rem;\n  border-radius: 100%;\n  border: 4px solid #c3bedc;\n  position: relative;\n  top: 0.13rem;\n  margin-right: 0.26rem;\n  background-color: #fff;\n}\n.donut.md {\n  width: 1.73rem;\n  height: 1.73rem;\n}\n.donut.default {\n  border-color: #c3bedc;\n}\n.donut.primary {\n  border-color: var(--kit-color-primary);\n}\n.donut.secondary {\n  border-color: #aca6cc;\n}\n.donut.success {\n  border-color: #41b883;\n}\n.donut.warning {\n  border-color: #f2a654;\n}\n.donut.danger {\n  border-color: #f00;\n}\n.donut.info {\n  border-color: #3d6ee7;\n}\n.donut.yellow {\n  border-color: #ff0;\n}\n.donut.orange {\n  border-color: #f2a654;\n}\n.donut.gray2 {\n  border-color: #e4e9f0;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/@kit/widgets/Charts/3/index.vue?vue&type=style&index=0&lang=scss&module=true&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-2!./node_modules/sass-loader/dist/cjs.js??ref--8-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/@kit/widgets/Charts/3/index.vue?vue&type=style&index=0&lang=scss&module=true& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/src??ref--8-2!../../../../../../node_modules/sass-loader/dist/cjs.js??ref--8-3!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=style&index=0&lang=scss&module=true& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/@kit/widgets/Charts/3/index.vue?vue&type=style&index=0&lang=scss&module=true&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/@kit/widgets/Charts/3/index.vue?vue&type=template&id=6c4cd9c7&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/@kit/widgets/Charts/3/index.vue?vue&type=template&id=6c4cd9c7& ***!
  \*******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("vue-chartist", {
        staticClass: "height-200 mb-3",
        attrs: {
          type: "Bar",
          data: _vm.data,
          options: _vm.options,
          listener: _vm.listener
        }
      }),
      _vm._v(" "),
      _c("div", { staticClass: "d-flex flex-wrap" }, [
        _c("div", { staticClass: "mr-5 mb-2" }, [
          _c("div", { staticClass: "text-nowrap text-uppercase text-gray-4" }, [
            _c("div", { class: [_vm.$style.donut, _vm.$style.success] }),
            _vm._v("Gross revenue\n      ")
          ]),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "font-weight-bold font-size-18 text-dark" },
            [_vm._v("+$125,367.36")]
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "mr-5 mb-2" }, [
          _c("div", { staticClass: "text-nowrap text-uppercase text-gray-4" }, [
            _c("div", { class: [_vm.$style.primary, _vm.$style.donut] }),
            _vm._v("Gross Earnings\n      ")
          ]),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "font-weight-bold font-size-18 text-dark" },
            [_vm._v("+$125,367.36")]
          )
        ])
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/@kit/widgets/Lists/11/index.vue?vue&type=template&id=42030b30&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/@kit/widgets/Lists/11/index.vue?vue&type=template&id=42030b30& ***!
  \*******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [
      _c("div", { staticClass: "mb-3" }, [
        _c(
          "div",
          { staticClass: "bg-light text-gray-6 text-uppercase px-3 py-1 mb-2" },
          [_vm._v("Today - 7 may 2019")]
        ),
        _vm._v(" "),
        _c("div", { staticClass: "table-responsive" }, [
          _c(
            "table",
            { staticClass: "table table-borderless text-gray-6 mb-0" },
            [
              _c("tbody", [
                _c("tr", [
                  _c("td", [_vm._v("California")]),
                  _vm._v(" "),
                  _c("td", { staticClass: "text-right" }, [
                    _c("strong", [_vm._v("+78,366,263.00$")])
                  ])
                ]),
                _vm._v(" "),
                _c("tr", [
                  _c("td", [_vm._v("Texas")]),
                  _vm._v(" "),
                  _c("td", { staticClass: "text-right" }, [
                    _c("strong", [_vm._v("+58,165,000.00$")])
                  ])
                ]),
                _vm._v(" "),
                _c("tr", [
                  _c("td", [_vm._v("Wyoming")]),
                  _vm._v(" "),
                  _c("td", { staticClass: "text-right" }, [
                    _c("strong", [_vm._v("+26,156,267.00$")])
                  ])
                ]),
                _vm._v(" "),
                _c("tr", [
                  _c("td", [_vm._v("Florida")]),
                  _vm._v(" "),
                  _c("td", { staticClass: "text-right" }, [
                    _c("strong", [_vm._v("+18,823,026.00$")])
                  ])
                ]),
                _vm._v(" "),
                _c("tr", [
                  _c("td", [_vm._v("New York")]),
                  _vm._v(" "),
                  _c("td", { staticClass: "text-right" }, [
                    _c("strong", [_vm._v("+8,125,642.00$")])
                  ])
                ])
              ])
            ]
          )
        ])
      ]),
      _vm._v(" "),
      _c("div", [
        _c(
          "div",
          { staticClass: "bg-light text-gray-6 text-uppercase px-3 py-1 mb-2" },
          [_vm._v("6 may 2019")]
        ),
        _vm._v(" "),
        _c("div", { staticClass: "table-responsive" }, [
          _c(
            "table",
            { staticClass: "table table-borderless text-muted mb-0" },
            [_c("tbody", [_c("tr", [_c("td", [_vm._v("No Items")])])])]
          )
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ecommerce/dashboard/index.vue?vue&type=template&id=4270b7d9&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/ecommerce/dashboard/index.vue?vue&type=template&id=4270b7d9& ***!
  \***********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _vm._m(0),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-xl-8 col-lg-12" }, [
        _c(
          "div",
          { staticClass: "card" },
          [
            _c(
              "a-tabs",
              {
                staticClass: "kit-tabs-bordered",
                attrs: { defaultActiveKey: "2" }
              },
              [
                _c("a-tab-pane", { key: "1", attrs: { tab: "Orders" } }),
                _vm._v(" "),
                _c("a-tab-pane", { key: "2", attrs: { tab: "Revenue" } })
              ],
              1
            ),
            _vm._v(" "),
            _c("div", { staticClass: "card-body" }, [_c("air-chart-3")], 1)
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "card" },
          [
            _c(
              "a-tabs",
              {
                staticClass: "kit-tabs-bordered",
                attrs: { defaultActiveKey: "1" }
              },
              [
                _c("a-tab-pane", { key: "1", attrs: { tab: "Bestsellers" } }),
                _vm._v(" "),
                _c("a-tab-pane", { key: "2", attrs: { tab: "Most Viewed" } }),
                _vm._v(" "),
                _c("a-tab-pane", { key: "3", attrs: { tab: "Highest Rated" } })
              ],
              1
            ),
            _vm._v(" "),
            _c("div", { staticClass: "card-body" }, [
              _c(
                "div",
                { staticClass: "row" },
                _vm._l(_vm.products, function(product, index) {
                  return _c(
                    "div",
                    { key: index, staticClass: "col-lg-6" },
                    [
                      _c("air-general-16", {
                        attrs: {
                          isNew: product.isNew,
                          isFavorite: product.isFavorite,
                          image: product.image,
                          name: product.name,
                          price: product.price,
                          oldPrice: product.oldPrice
                        }
                      })
                    ],
                    1
                  )
                }),
                0
              )
            ])
          ],
          1
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-xl-4 col-lg-12" }, [
        _c("div", { staticClass: "card text-white bg-success" }, [
          _c("div", { staticClass: "card-body" }, [_c("air-general-2v3")], 1)
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "card" }, [
          _c("div", { staticClass: "card-body" }, [_c("air-general-2")], 1)
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "card" }, [
          _c("div", { staticClass: "card-body" }, [_c("air-general-2v1")], 1)
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "card" }, [
          _c("div", { staticClass: "card-body" }, [_c("air-general-2v2")], 1)
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "card" }, [
          _c("div", { staticClass: "card-body" }, [_c("air-list-11")], 1)
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "card" }, [
          _c("div", { staticClass: "card-body" }, [_c("air-list-12")], 1)
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "air__utils__heading" }, [
      _c("strong", [_vm._v("Ecommerce: Dashboard")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/@kit/widgets/Charts/3/data.json":
/*!******************************************************!*\
  !*** ./resources/js/@kit/widgets/Charts/3/data.json ***!
  \******************************************************/
/*! exports provided: labels, series, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"labels\":[\"Jan\",\"Feb\",\"Mar\",\"Apr\",\"May\",\"Jun\",\"Jul\",\"Aug\",\"Sep\",\"Oct\",\"Nov\",\"Dec\"],\"series\":[{\"className\":\"ct-series-a\",\"data\":[5,4,3,7,5,10,3,4,8,10,6,8]},{\"className\":\"ct-series-b\",\"data\":[3,2,9,5,4,6,4,6,7,8,7,4]}]}");

/***/ }),

/***/ "./resources/js/@kit/widgets/Charts/3/index.vue":
/*!******************************************************!*\
  !*** ./resources/js/@kit/widgets/Charts/3/index.vue ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var _index_vue_vue_type_template_id_6c4cd9c7___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.vue?vue&type=template&id=6c4cd9c7& */ "./resources/js/@kit/widgets/Charts/3/index.vue?vue&type=template&id=6c4cd9c7&");
/* harmony import */ var _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.vue?vue&type=script&lang=js& */ "./resources/js/@kit/widgets/Charts/3/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _index_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./index.vue?vue&type=style&index=0&lang=scss&module=true& */ "./resources/js/@kit/widgets/Charts/3/index.vue?vue&type=style&index=0&lang=scss&module=true&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





var cssModules = {}
var disposed = false

function injectStyles (context) {
  if (disposed) return
  
        cssModules["$style"] = (_index_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_2__["default"].locals || _index_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_2__["default"])
        Object.defineProperty(this, "$style", {
          configurable: true,
          get: function () {
            return cssModules["$style"]
          }
        })
      
}


  module.hot && false



        module.hot && false

/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _index_vue_vue_type_template_id_6c4cd9c7___WEBPACK_IMPORTED_MODULE_0__["render"],
  _index_vue_vue_type_template_id_6c4cd9c7___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  injectStyles,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/@kit/widgets/Charts/3/index.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./resources/js/@kit/widgets/Charts/3/index.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./resources/js/@kit/widgets/Charts/3/index.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/@kit/widgets/Charts/3/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/@kit/widgets/Charts/3/index.vue?vue&type=style&index=0&lang=scss&module=true&":
/*!****************************************************************************************************!*\
  !*** ./resources/js/@kit/widgets/Charts/3/index.vue?vue&type=style&index=0&lang=scss&module=true& ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/style-loader!../../../../../../node_modules/css-loader!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/src??ref--8-2!../../../../../../node_modules/sass-loader/dist/cjs.js??ref--8-3!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=style&index=0&lang=scss&module=true& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/@kit/widgets/Charts/3/index.vue?vue&type=style&index=0&lang=scss&module=true&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (default from non-harmony) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_0___default.a; });
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 

/***/ }),

/***/ "./resources/js/@kit/widgets/Charts/3/index.vue?vue&type=template&id=6c4cd9c7&":
/*!*************************************************************************************!*\
  !*** ./resources/js/@kit/widgets/Charts/3/index.vue?vue&type=template&id=6c4cd9c7& ***!
  \*************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_6c4cd9c7___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=template&id=6c4cd9c7& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/@kit/widgets/Charts/3/index.vue?vue&type=template&id=6c4cd9c7&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_6c4cd9c7___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_6c4cd9c7___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/@kit/widgets/Lists/11/index.vue":
/*!******************************************************!*\
  !*** ./resources/js/@kit/widgets/Lists/11/index.vue ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _index_vue_vue_type_template_id_42030b30___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.vue?vue&type=template&id=42030b30& */ "./resources/js/@kit/widgets/Lists/11/index.vue?vue&type=template&id=42030b30&");
/* harmony import */ var _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.vue?vue&type=script&lang=js& */ "./resources/js/@kit/widgets/Lists/11/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _index_vue_vue_type_template_id_42030b30___WEBPACK_IMPORTED_MODULE_0__["render"],
  _index_vue_vue_type_template_id_42030b30___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/@kit/widgets/Lists/11/index.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/@kit/widgets/Lists/11/index.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./resources/js/@kit/widgets/Lists/11/index.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/@kit/widgets/Lists/11/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/@kit/widgets/Lists/11/index.vue?vue&type=template&id=42030b30&":
/*!*************************************************************************************!*\
  !*** ./resources/js/@kit/widgets/Lists/11/index.vue?vue&type=template&id=42030b30& ***!
  \*************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_42030b30___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=template&id=42030b30& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/@kit/widgets/Lists/11/index.vue?vue&type=template&id=42030b30&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_42030b30___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_42030b30___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/ecommerce/dashboard/data.json":
/*!**********************************************************!*\
  !*** ./resources/js/views/ecommerce/dashboard/data.json ***!
  \**********************************************************/
/*! exports provided: 0, 1, 2, 3, default */
/***/ (function(module) {

module.exports = JSON.parse("[{\"isNew\":true,\"isFavorite\":false,\"image\":\"resources/images/products/001.jpg\",\"name\":\"TP-Link AC1750 Smart WiFi Router - Dual Band Gigabit Wireless Internet Routers for Home\",\"price\":\"199.28\",\"oldPrice\":\"299.28\"},{\"isNew\":false,\"isFavorite\":true,\"image\":\"resources/images/products/002.jpg\",\"name\":\"TP-Link AC1750 Smart WiFi Router - Dual Band Gigabit Wireless Internet Routers for Home\",\"price\":\"99.99\",\"oldPrice\":\"199.99\"},{\"isNew\":false,\"isFavorite\":false,\"image\":\"resources/images/products/003.jpg\",\"name\":\"TP-Link AC1750 Smart WiFi Router - Dual Band Gigabit Wireless Internet Routers for Home\",\"price\":\"245.21\",\"oldPrice\":\"419.58\"},{\"isNew\":true,\"isFavorite\":true,\"image\":\"resources/images/products/004.jpg\",\"name\":\"TP-Link AC1750 Smart WiFi Router - Dual Band Gigabit Wireless Internet Routers for Home\",\"price\":\"199.99\",\"oldPrice\":\"499.99\"}]");

/***/ }),

/***/ "./resources/js/views/ecommerce/dashboard/index.vue":
/*!**********************************************************!*\
  !*** ./resources/js/views/ecommerce/dashboard/index.vue ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _index_vue_vue_type_template_id_4270b7d9___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.vue?vue&type=template&id=4270b7d9& */ "./resources/js/views/ecommerce/dashboard/index.vue?vue&type=template&id=4270b7d9&");
/* harmony import */ var _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.vue?vue&type=script&lang=js& */ "./resources/js/views/ecommerce/dashboard/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _index_vue_vue_type_template_id_4270b7d9___WEBPACK_IMPORTED_MODULE_0__["render"],
  _index_vue_vue_type_template_id_4270b7d9___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/ecommerce/dashboard/index.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/ecommerce/dashboard/index.vue?vue&type=script&lang=js&":
/*!***********************************************************************************!*\
  !*** ./resources/js/views/ecommerce/dashboard/index.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ecommerce/dashboard/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/ecommerce/dashboard/index.vue?vue&type=template&id=4270b7d9&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/views/ecommerce/dashboard/index.vue?vue&type=template&id=4270b7d9& ***!
  \*****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_4270b7d9___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=template&id=4270b7d9& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ecommerce/dashboard/index.vue?vue&type=template&id=4270b7d9&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_4270b7d9___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_4270b7d9___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);