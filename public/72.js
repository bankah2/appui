(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[72],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/analytics/index.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/analytics/index.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _kit_widgets_Charts_1_index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/@kit/widgets/Charts/1/index */ "./resources/js/@kit/widgets/Charts/1/index.vue");
/* harmony import */ var _kit_widgets_Charts_2_index__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/@kit/widgets/Charts/2/index */ "./resources/js/@kit/widgets/Charts/2/index.vue");
/* harmony import */ var _kit_widgets_Charts_5_index__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/@kit/widgets/Charts/5/index */ "./resources/js/@kit/widgets/Charts/5/index.vue");
/* harmony import */ var _kit_widgets_Charts_9_index__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/@kit/widgets/Charts/9/index */ "./resources/js/@kit/widgets/Charts/9/index.vue");
/* harmony import */ var _kit_widgets_Charts_10_index__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/@kit/widgets/Charts/10/index */ "./resources/js/@kit/widgets/Charts/10/index.vue");
/* harmony import */ var _kit_widgets_Lists_12_index__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @/@kit/widgets/Lists/12/index */ "./resources/js/@kit/widgets/Lists/12/index.vue");
/* harmony import */ var _kit_widgets_Lists_15_index__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @/@kit/widgets/Lists/15/index */ "./resources/js/@kit/widgets/Lists/15/index.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//







/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    AirChart1: _kit_widgets_Charts_1_index__WEBPACK_IMPORTED_MODULE_0__["default"],
    AirChart2: _kit_widgets_Charts_2_index__WEBPACK_IMPORTED_MODULE_1__["default"],
    AirChart5: _kit_widgets_Charts_5_index__WEBPACK_IMPORTED_MODULE_2__["default"],
    AirChart9: _kit_widgets_Charts_9_index__WEBPACK_IMPORTED_MODULE_3__["default"],
    AirChart10: _kit_widgets_Charts_10_index__WEBPACK_IMPORTED_MODULE_4__["default"],
    AirList12: _kit_widgets_Lists_12_index__WEBPACK_IMPORTED_MODULE_5__["default"],
    AirList15: _kit_widgets_Lists_15_index__WEBPACK_IMPORTED_MODULE_6__["default"]
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/analytics/index.vue?vue&type=template&id=6f59b6c2&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/analytics/index.vue?vue&type=template&id=6f59b6c2& ***!
  \***********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _vm._m(0),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-xl-8 col-lg-6" }, [
        _c("h5", { staticClass: "text-dark mb-4" }, [
          _vm._v("Google Analytics Home")
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "card" }, [_c("air-chart-2")], 1),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-xl-6 col-lg-12" }, [
            _c("div", { staticClass: "card" }, [
              _c("div", { staticClass: "card-body" }, [_c("air-chart-9")], 1)
            ]),
            _vm._v(" "),
            _c("h5", { staticClass: "text-dark mb-4" }, [
              _vm._v("How do you acquire users?")
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "card" }, [
              _c("div", { staticClass: "card-body" }, [_c("air-chart-5")], 1)
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-xl-6 col-lg-12" }, [
            _c("div", { staticClass: "card" }, [
              _c("div", { staticClass: "card-body" }, [_c("air-chart-10")], 1)
            ]),
            _vm._v(" "),
            _c("h5", { staticClass: "text-dark mb-4" }, [
              _vm._v("How are your active users trending over time?")
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "card" }, [
              _c("div", { staticClass: "card-body" }, [_c("air-chart-1")], 1)
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-xl-4 col-lg-6" }, [
        _c("h5", { staticClass: "text-dark mb-4" }, [
          _vm._v("Ask analytics Intelligence")
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "card" }, [
          _c("div", { staticClass: "card-body" }, [_c("air-list-15")], 1)
        ]),
        _vm._v(" "),
        _c("h5", { staticClass: "text-dark mb-4" }, [
          _vm._v("What are your top devices?")
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "card" }, [
          _c("div", { staticClass: "card-body" }, [_c("air-list-12")], 1)
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "air__utils__heading" }, [
      _c("h5", [_vm._v("Dashboard Analytics")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/dashboard/analytics/index.vue":
/*!**********************************************************!*\
  !*** ./resources/js/views/dashboard/analytics/index.vue ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _index_vue_vue_type_template_id_6f59b6c2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.vue?vue&type=template&id=6f59b6c2& */ "./resources/js/views/dashboard/analytics/index.vue?vue&type=template&id=6f59b6c2&");
/* harmony import */ var _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.vue?vue&type=script&lang=js& */ "./resources/js/views/dashboard/analytics/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _index_vue_vue_type_template_id_6f59b6c2___WEBPACK_IMPORTED_MODULE_0__["render"],
  _index_vue_vue_type_template_id_6f59b6c2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/dashboard/analytics/index.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/dashboard/analytics/index.vue?vue&type=script&lang=js&":
/*!***********************************************************************************!*\
  !*** ./resources/js/views/dashboard/analytics/index.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/analytics/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/dashboard/analytics/index.vue?vue&type=template&id=6f59b6c2&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/views/dashboard/analytics/index.vue?vue&type=template&id=6f59b6c2& ***!
  \*****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_6f59b6c2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=template&id=6f59b6c2& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/analytics/index.vue?vue&type=template&id=6f59b6c2&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_6f59b6c2___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_6f59b6c2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);