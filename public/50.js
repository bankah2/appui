(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[50],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/apps/messaging/index.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/apps/messaging/index.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_custom_scrollbar__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-custom-scrollbar */ "./node_modules/vue-custom-scrollbar/dist/vueScrollbar.umd.min.js");
/* harmony import */ var vue_custom_scrollbar__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue_custom_scrollbar__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _data_json__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./data.json */ "./resources/js/views/apps/messaging/data.json");
var _data_json__WEBPACK_IMPORTED_MODULE_1___namespace = /*#__PURE__*/__webpack_require__.t(/*! ./data.json */ "./resources/js/views/apps/messaging/data.json", 1);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    vueCustomScrollbar: vue_custom_scrollbar__WEBPACK_IMPORTED_MODULE_0___default.a
  },
  data: function data() {
    var activeIndex = 0;
    return {
      activeIndex: activeIndex,
      dialogs: _data_json__WEBPACK_IMPORTED_MODULE_1__,
      name: _data_json__WEBPACK_IMPORTED_MODULE_1__[activeIndex].name,
      position: _data_json__WEBPACK_IMPORTED_MODULE_1__[activeIndex].position,
      dialog: _data_json__WEBPACK_IMPORTED_MODULE_1__[activeIndex].dialog,
      avatar: _data_json__WEBPACK_IMPORTED_MODULE_1__[activeIndex].avatar
    };
  },
  methods: {
    changeDialog: function changeDialog(index) {
      this.activeIndex = index;
      this.name = _data_json__WEBPACK_IMPORTED_MODULE_1__[index].name;
      this.position = _data_json__WEBPACK_IMPORTED_MODULE_1__[index].position;
      this.dialog = _data_json__WEBPACK_IMPORTED_MODULE_1__[index].dialog;
      this.avatar = _data_json__WEBPACK_IMPORTED_MODULE_1__[index].avatar;
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/apps/messaging/index.vue?vue&type=style&index=0&lang=scss&module=true&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-2!./node_modules/sass-loader/dist/cjs.js??ref--8-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/apps/messaging/index.vue?vue&type=style&index=0&lang=scss&module=true& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".dialogs {\n  height: 100%;\n}\n@media (max-width: 575px) {\n.dialogs {\n    max-height: 16rem;\n    margin-bottom: 1rem;\n}\n}\n.item {\n  padding: 0.66rem;\n  cursor: pointer;\n  border-radius: 5px;\n  margin-bottom: 1rem;\n}\n.item:last-child {\n  margin-bottom: 0;\n}\n.item:hover {\n  background-color: #f9fafc;\n}\n.current {\n  background-color: #f2f4f8;\n}\n.current:hover {\n  background-color: #f2f4f8;\n}\n.info {\n  min-width: 0;\n}\n.unread {\n  min-width: 15px;\n}\n.message {\n  display: flex;\n  flex-direction: row;\n  flex-wrap: nowrap;\n  align-items: flex-end;\n  justify-content: flex-end;\n  margin-top: 1rem;\n  overflow: hidden;\n  flex-shrink: 0;\n}\n.message.answer {\n  flex-direction: row-reverse;\n}\n.message.answer .messageAvatar {\n  margin-left: 0;\n  margin-right: 1rem;\n}\n.message.answer .messageContent::before {\n  left: auto;\n  right: 100%;\n  border-top: 5px solid transparent;\n  border-bottom: 5px solid transparent;\n  border-right: 4px solid #f2f4f8;\n  border-left: none;\n}\n.messageAvatar {\n  flex-shrink: 0;\n  margin-left: 1rem;\n}\n.messageContent {\n  background-color: #f2f4f8;\n  position: relative;\n  border-radius: 5px;\n  padding-left: 1rem;\n  padding-right: 1rem;\n  padding-top: 0.4rem;\n  padding-bottom: 0.4rem;\n}\n.messageContent::before {\n  content: \"\";\n  position: absolute;\n  left: 100%;\n  bottom: 16px;\n  width: 0;\n  height: 0;\n  border-top: 5px solid transparent;\n  border-bottom: 5px solid transparent;\n  border-left: 4px solid #f2f4f8;\n  border-right: none;\n}\n[data-kit-theme=dark] .item:hover, [data-kit-theme=dark] .item.current {\n  background-color: #2a274d;\n}\n[data-kit-theme=dark] .messageContent {\n  background-color: #161537;\n}\n[data-kit-theme=dark] .messageContent:before {\n  border-left-color: #161537;\n}\n[data-kit-theme=dark] .message.answer .messageContent:before {\n  border-right-color: #161537;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/apps/messaging/index.vue?vue&type=style&index=0&lang=scss&module=true&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-2!./node_modules/sass-loader/dist/cjs.js??ref--8-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/apps/messaging/index.vue?vue&type=style&index=0&lang=scss&module=true& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--8-2!../../../../../node_modules/sass-loader/dist/cjs.js??ref--8-3!../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=style&index=0&lang=scss&module=true& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/apps/messaging/index.vue?vue&type=style&index=0&lang=scss&module=true&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/apps/messaging/index.vue?vue&type=template&id=442e862e&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/apps/messaging/index.vue?vue&type=template&id=442e862e& ***!
  \******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-12 col-md-3" }, [
        _c(
          "div",
          { staticClass: "mb-4" },
          [
            _c(
              "a-input",
              { attrs: { placeholder: "Search users" } },
              [
                _c("a-icon", {
                  attrs: { slot: "prefix", type: "search" },
                  slot: "prefix"
                })
              ],
              1
            )
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "div",
          { class: _vm.$style.dialogs },
          [
            _c(
              "vue-custom-scrollbar",
              { staticStyle: { height: "100%" } },
              _vm._l(_vm.dialogs, function(item, index) {
                return _c(
                  "a",
                  {
                    key: item.name,
                    staticClass: "d-flex flex-nowrap align-items-center",
                    class: [
                      _vm.$style.item,
                      index === _vm.activeIndex ? _vm.$style.current : ""
                    ],
                    attrs: { href: "#" },
                    on: {
                      click: function($event) {
                        $event.stopPropagation()
                        $event.preventDefault()
                        return _vm.changeDialog(index)
                      }
                    }
                  },
                  [
                    _c(
                      "div",
                      {
                        staticClass:
                          "kit__utils__avatar kit__utils__avatar--size46 mr-3 flex-shrink-0"
                      },
                      [
                        _c("img", {
                          attrs: { src: item.avatar, alt: item.name }
                        })
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "flex-grow-1", class: _vm.$style.info },
                      [
                        _c(
                          "div",
                          {
                            staticClass:
                              "text-uppercase font-size-12 text-truncate text-gray-6"
                          },
                          [_vm._v(_vm._s(item.position))]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            staticClass:
                              "text-dark font-size-18 font-weight-bold text-truncate"
                          },
                          [_vm._v(_vm._s(item.name))]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    item.unread
                      ? _c(
                          "div",
                          {
                            staticClass: "flex-shrink-0 align-self-start",
                            class: _vm.$style.unread
                          },
                          [
                            _c("div", { staticClass: "badge badge-success" }, [
                              _vm._v(_vm._s(item.unread))
                            ])
                          ]
                        )
                      : _vm._e()
                  ]
                )
              }),
              0
            )
          ],
          1
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-12 col-md-9" }, [
        _c("div", { staticClass: "card" }, [
          _c(
            "div",
            { staticClass: "card-header card-header-flex align-items-center" },
            [
              _c(
                "div",
                {
                  staticClass:
                    "d-flex flex-column justify-content-center mr-auto"
                },
                [
                  _c("h5", { staticClass: "mb-0 mr-2 font-size-18" }, [
                    _vm._v(
                      "\n              " + _vm._s(_vm.name) + "\n              "
                    ),
                    _c("span", { staticClass: "font-size-14 text-gray-6" }, [
                      _vm._v("(" + _vm._s(_vm.position) + ")")
                    ])
                  ])
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                [
                  _c(
                    "a-tooltip",
                    { attrs: { placement: "top" } },
                    [
                      _c("template", { slot: "title" }, [
                        _c("span", [_vm._v("Unlock Account")])
                      ]),
                      _vm._v(" "),
                      _c(
                        "a",
                        {
                          staticClass: "btn btn-sm btn-light mr-2",
                          attrs: { href: "javascript: void(0);" }
                        },
                        [_c("i", { staticClass: "fe fe-unlock" })]
                      )
                    ],
                    2
                  ),
                  _vm._v(" "),
                  _c(
                    "a-tooltip",
                    { attrs: { placement: "top" } },
                    [
                      _c("template", { slot: "title" }, [
                        _c("span", [_vm._v("Mark as important")])
                      ]),
                      _vm._v(" "),
                      _c(
                        "a",
                        {
                          staticClass: "btn btn-sm btn-light mr-2",
                          attrs: { href: "javascript: void(0);" }
                        },
                        [_c("i", { staticClass: "fe fe-star" })]
                      )
                    ],
                    2
                  ),
                  _vm._v(" "),
                  _c(
                    "a-tooltip",
                    { attrs: { placement: "top" } },
                    [
                      _c("template", { slot: "title" }, [
                        _c("span", [_vm._v("Delete user")])
                      ]),
                      _vm._v(" "),
                      _c(
                        "a",
                        {
                          staticClass: "btn btn-sm btn-light mr-2",
                          attrs: { href: "javascript: void(0);" }
                        },
                        [_c("i", { staticClass: "fe fe-trash" })]
                      )
                    ],
                    2
                  )
                ],
                1
              )
            ]
          ),
          _vm._v(" "),
          _c("div", { staticClass: "card-body" }, [
            _c(
              "div",
              { staticClass: "height-700" },
              [
                _c(
                  "vue-custom-scrollbar",
                  { staticStyle: { height: "100%" } },
                  [
                    _c(
                      "div",
                      {
                        staticClass:
                          "d-flex flex-column justify-content-end height-100p"
                      },
                      _vm._l(_vm.dialog, function(message, index) {
                        return _c(
                          "div",
                          {
                            key: index,
                            class: [
                              _vm.$style.message,
                              message.owner !== "you" ? _vm.$style.answer : ""
                            ]
                          },
                          [
                            _c("div", { class: _vm.$style.messageContent }, [
                              _c(
                                "div",
                                {
                                  staticClass:
                                    "text-gray-4 font-size-12 text-uppercase"
                                },
                                [
                                  _vm._v(
                                    _vm._s(message.owner) +
                                      ", " +
                                      _vm._s(message.time)
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c("div", [_vm._v(_vm._s(message.content))])
                            ]),
                            _vm._v(" "),
                            _c(
                              "div",
                              {
                                staticClass: "kit__utils__avatar",
                                class: _vm.$style.messageAvatar
                              },
                              [
                                _c("img", {
                                  attrs: {
                                    src:
                                      message.owner !== "you"
                                        ? _vm.avatar
                                        : "resources/images/avatars/avatar-2.png",
                                    alt: _vm.name
                                  }
                                })
                              ]
                            )
                          ]
                        )
                      }),
                      0
                    )
                  ]
                )
              ],
              1
            ),
            _vm._v(" "),
            _c("div", { staticClass: "pt-2 pb-2" }, [
              _vm._v(_vm._s(_vm.name) + " is typing...")
            ]),
            _vm._v(" "),
            _vm._m(0)
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group mb-3" }, [
      _c("input", {
        staticClass: "form-control",
        attrs: { type: "text", placeholder: "Send message..." }
      }),
      _vm._v(" "),
      _c("div", { staticClass: "input-group-append" }, [
        _c(
          "button",
          { staticClass: "btn btn-primary", attrs: { type: "button" } },
          [_c("i", { staticClass: "fe fe-send align-middle" })]
        )
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/apps/messaging/data.json":
/*!*****************************************************!*\
  !*** ./resources/js/views/apps/messaging/data.json ***!
  \*****************************************************/
/*! exports provided: 0, 1, 2, 3, default */
/***/ (function(module) {

module.exports = JSON.parse("[{\"name\":\"Hellen Maggie\",\"avatar\":\"resources/images/avatars/3.jpg\",\"unread\":1,\"position\":\"Administrator\",\"dialog\":[{\"owner\":\"you\",\"time\":\"5 min ago\",\"content\":\"Hi! Anyone here? I want to know how I can buy Kit UI?\"},{\"owner\":\"hellen\",\"time\":\"5 min ago\",\"content\":\"Please call us + 100 295 000\"}]},{\"name\":\"Dean Beckham\",\"avatar\":\"resources/images/avatars/1.jpg\",\"unread\":2,\"position\":\"Moderator\",\"dialog\":[]},{\"name\":\"Mike Stake\",\"avatar\":\"resources/images/avatars/4.jpg\",\"unread\":0,\"position\":\"Agent\",\"dialog\":[]},{\"name\":\"Janie Lawrence\",\"avatar\":\"resources/images/avatars/5.jpg\",\"unread\":0,\"position\":\"Agent\",\"dialog\":[]}]");

/***/ }),

/***/ "./resources/js/views/apps/messaging/index.vue":
/*!*****************************************************!*\
  !*** ./resources/js/views/apps/messaging/index.vue ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var _index_vue_vue_type_template_id_442e862e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.vue?vue&type=template&id=442e862e& */ "./resources/js/views/apps/messaging/index.vue?vue&type=template&id=442e862e&");
/* harmony import */ var _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.vue?vue&type=script&lang=js& */ "./resources/js/views/apps/messaging/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _index_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./index.vue?vue&type=style&index=0&lang=scss&module=true& */ "./resources/js/views/apps/messaging/index.vue?vue&type=style&index=0&lang=scss&module=true&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





var cssModules = {}
var disposed = false

function injectStyles (context) {
  if (disposed) return
  
        cssModules["$style"] = (_index_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_2__["default"].locals || _index_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_2__["default"])
        Object.defineProperty(this, "$style", {
          configurable: true,
          get: function () {
            return cssModules["$style"]
          }
        })
      
}


  module.hot && false



        module.hot && false

/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _index_vue_vue_type_template_id_442e862e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _index_vue_vue_type_template_id_442e862e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  injectStyles,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/apps/messaging/index.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./resources/js/views/apps/messaging/index.vue?vue&type=script&lang=js&":
/*!******************************************************************************!*\
  !*** ./resources/js/views/apps/messaging/index.vue?vue&type=script&lang=js& ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/apps/messaging/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/apps/messaging/index.vue?vue&type=style&index=0&lang=scss&module=true&":
/*!***************************************************************************************************!*\
  !*** ./resources/js/views/apps/messaging/index.vue?vue&type=style&index=0&lang=scss&module=true& ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--8-2!../../../../../node_modules/sass-loader/dist/cjs.js??ref--8-3!../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=style&index=0&lang=scss&module=true& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/apps/messaging/index.vue?vue&type=style&index=0&lang=scss&module=true&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (default from non-harmony) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_0___default.a; });
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 

/***/ }),

/***/ "./resources/js/views/apps/messaging/index.vue?vue&type=template&id=442e862e&":
/*!************************************************************************************!*\
  !*** ./resources/js/views/apps/messaging/index.vue?vue&type=template&id=442e862e& ***!
  \************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_442e862e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=template&id=442e862e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/apps/messaging/index.vue?vue&type=template&id=442e862e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_442e862e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_442e862e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);