(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[5],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/@kit/widgets/General/15/index.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/@kit/widgets/General/15/index.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'KitGeneral15'
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/@kit/widgets/General/15/index.vue?vue&type=template&id=2300ae39&":
/*!*********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/@kit/widgets/General/15/index.vue?vue&type=template&id=2300ae39& ***!
  \*********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "d-flex flex-nowrap align-items-start pt-4" },
    [
      _vm._m(0),
      _vm._v(" "),
      _c("div", { staticClass: "flex-grow-1" }, [
        _c("div", { staticClass: "border-bottom" }, [
          _c("div", { staticClass: "d-flex flex-wrap mb-2" }, [
            _vm._m(1),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "nav-item dropdown" },
              [
                _c(
                  "a-dropdown",
                  { attrs: { placement: "bottomRight", trigger: ["click"] } },
                  [
                    _c(
                      "a",
                      {
                        staticClass: "nav-link dropdown-toggle pt-sm-0",
                        attrs: { href: "javascript: void(0);" }
                      },
                      [_vm._v("Actions")]
                    ),
                    _vm._v(" "),
                    _c(
                      "a-menu",
                      { attrs: { slot: "overlay" }, slot: "overlay" },
                      [
                        _c("a-menu-item", [
                          _c("a", { attrs: { href: "javascript:;" } }, [
                            _vm._v("Action")
                          ])
                        ]),
                        _vm._v(" "),
                        _c("a-menu-item", [
                          _c("a", { attrs: { href: "javascript:;" } }, [
                            _vm._v("Another action")
                          ])
                        ]),
                        _vm._v(" "),
                        _c("a-menu-item", [
                          _c("a", { attrs: { href: "javascript:;" } }, [
                            _vm._v("Something else here")
                          ])
                        ]),
                        _vm._v(" "),
                        _c("a-menu-divider"),
                        _vm._v(" "),
                        _c("a-menu-item", [
                          _c("a", { attrs: { href: "javascript:;" } }, [
                            _vm._v("Separated link")
                          ])
                        ])
                      ],
                      1
                    )
                  ],
                  1
                )
              ],
              1
            )
          ]),
          _vm._v(" "),
          _vm._m(2),
          _vm._v(" "),
          _vm._m(3)
        ]),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "d-flex flex-nowrap align-items-start pt-4" },
          [
            _vm._m(4),
            _vm._v(" "),
            _c("div", { staticClass: "flex-grow-1" }, [
              _c("div", { staticClass: "border-bottom" }, [
                _c("div", { staticClass: "d-flex flex-wrap mb-2" }, [
                  _vm._m(5),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "nav-item dropdown" },
                    [
                      _c(
                        "a-dropdown",
                        {
                          attrs: {
                            placement: "bottomRight",
                            trigger: ["click"]
                          }
                        },
                        [
                          _c(
                            "a",
                            {
                              staticClass: "nav-link dropdown-toggle pt-sm-0",
                              attrs: { href: "javascript: void(0);" }
                            },
                            [_vm._v("Actions")]
                          ),
                          _vm._v(" "),
                          _c(
                            "a-menu",
                            { attrs: { slot: "overlay" }, slot: "overlay" },
                            [
                              _c("a-menu-item", [
                                _c("a", { attrs: { href: "javascript:;" } }, [
                                  _vm._v("Action")
                                ])
                              ]),
                              _vm._v(" "),
                              _c("a-menu-item", [
                                _c("a", { attrs: { href: "javascript:;" } }, [
                                  _vm._v("Another action")
                                ])
                              ]),
                              _vm._v(" "),
                              _c("a-menu-item", [
                                _c("a", { attrs: { href: "javascript:;" } }, [
                                  _vm._v("Something else here")
                                ])
                              ]),
                              _vm._v(" "),
                              _c("a-menu-divider"),
                              _vm._v(" "),
                              _c("a-menu-item", [
                                _c("a", { attrs: { href: "javascript:;" } }, [
                                  _vm._v("Separated link")
                                ])
                              ])
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ]),
                _vm._v(" "),
                _vm._m(6),
                _vm._v(" "),
                _vm._m(7)
              ])
            ])
          ]
        ),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "d-flex flex-nowrap align-items-start pt-4" },
          [
            _vm._m(8),
            _vm._v(" "),
            _c("div", { staticClass: "flex-grow-1" }, [
              _c("div", { staticClass: "border-bottom" }, [
                _c("div", { staticClass: "d-flex flex-wrap mb-2" }, [
                  _vm._m(9),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "nav-item dropdown" },
                    [
                      _c(
                        "a-dropdown",
                        {
                          attrs: {
                            placement: "bottomRight",
                            trigger: ["click"]
                          }
                        },
                        [
                          _c(
                            "a",
                            {
                              staticClass: "nav-link dropdown-toggle pt-sm-0",
                              attrs: { href: "javascript: void(0);" }
                            },
                            [_vm._v("Actions")]
                          ),
                          _vm._v(" "),
                          _c(
                            "a-menu",
                            { attrs: { slot: "overlay" }, slot: "overlay" },
                            [
                              _c("a-menu-item", [
                                _c("a", { attrs: { href: "javascript:;" } }, [
                                  _vm._v("Action")
                                ])
                              ]),
                              _vm._v(" "),
                              _c("a-menu-item", [
                                _c("a", { attrs: { href: "javascript:;" } }, [
                                  _vm._v("Another action")
                                ])
                              ]),
                              _vm._v(" "),
                              _c("a-menu-item", [
                                _c("a", { attrs: { href: "javascript:;" } }, [
                                  _vm._v("Something else here")
                                ])
                              ]),
                              _vm._v(" "),
                              _c("a-menu-divider"),
                              _vm._v(" "),
                              _c("a-menu-item", [
                                _c("a", { attrs: { href: "javascript:;" } }, [
                                  _vm._v("Separated link")
                                ])
                              ])
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ]),
                _vm._v(" "),
                _vm._m(10),
                _vm._v(" "),
                _vm._m(11)
              ])
            ])
          ]
        )
      ])
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass:
          "mr-4 flex-shrink-0 align-self-start kit__utils__avatar kit__utils__avatar--size64"
      },
      [
        _c("img", {
          attrs: { src: "resources/images/avatars/3.jpg", alt: "Mary Stanform" }
        })
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "mr-auto" }, [
      _c("div", { staticClass: "text-gray-6" }, [
        _c("span", { staticClass: "text-dark font-weight-bold" }, [
          _vm._v("Helen maggie")
        ]),
        _vm._v(" posted\n          ")
      ]),
      _vm._v(" "),
      _c("div", [_vm._v("Few seconds ago")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "mb-3" }, [
      _vm._v(
        "\n        Lorem ipsum dolor sit amit,consectetur eiusmdd tempory\n        "
      ),
      _c("br"),
      _vm._v("incididunt ut labore et dolore magna elit\n      ")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass:
          "d-flex flex-wrap justify-content-start align-items-start mb-3"
      },
      [
        _c(
          "a",
          {
            staticClass: "text-blue mr-3",
            attrs: { href: "javascript: void(0);" }
          },
          [
            _c("i", { staticClass: "fe fe-heart mr-1" }),
            _vm._v(" 61 Likes\n        ")
          ]
        ),
        _vm._v(" "),
        _c(
          "a",
          {
            staticClass: "text-blue mr-3",
            attrs: { href: "javascript: void(0);" }
          },
          [
            _c("i", { staticClass: "fe fe-message-square mr-1" }),
            _vm._v(" 2 Comments\n        ")
          ]
        )
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass:
          "mr-4 flex-shrink-0 align-self-start kit__utils__avatar kit__utils__avatar--size64"
      },
      [
        _c("img", {
          attrs: { src: "resources/images/avatars/3.jpg", alt: "Mary Stanform" }
        })
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "mr-auto" }, [
      _c("div", { staticClass: "text-gray-6" }, [
        _c("span", { staticClass: "text-dark font-weight-bold" }, [
          _vm._v("Helen maggie")
        ]),
        _vm._v(" posted\n              ")
      ]),
      _vm._v(" "),
      _c("div", [_vm._v("Few seconds ago")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "mb-3" }, [
      _vm._v(
        "\n            Lorem ipsum dolor sit amit,consectetur eiusmdd tempory\n            "
      ),
      _c("br"),
      _vm._v("incididunt ut labore et dolore magna elit\n          ")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass:
          "d-flex flex-wrap justify-content-start align-items-start mb-3"
      },
      [
        _c(
          "a",
          {
            staticClass: "text-blue mr-3",
            attrs: { href: "javascript: void(0);" }
          },
          [
            _c("i", { staticClass: "fe fe-heart mr-1" }),
            _vm._v(" 61 Likes\n            ")
          ]
        ),
        _vm._v(" "),
        _c(
          "a",
          {
            staticClass: "text-blue mr-3",
            attrs: { href: "javascript: void(0);" }
          },
          [
            _c("i", { staticClass: "fe fe-message-square mr-1" }),
            _vm._v(" 2 Comments\n            ")
          ]
        )
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass:
          "mr-4 flex-shrink-0 align-self-start kit__utils__avatar kit__utils__avatar--size64"
      },
      [
        _c("img", {
          attrs: { src: "resources/images/avatars/3.jpg", alt: "Mary Stanform" }
        })
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "mr-auto" }, [
      _c("div", { staticClass: "text-gray-6" }, [
        _c("span", { staticClass: "text-dark font-weight-bold" }, [
          _vm._v("Helen maggie")
        ]),
        _vm._v(" posted\n              ")
      ]),
      _vm._v(" "),
      _c("div", [_vm._v("Few seconds ago")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "mb-3" }, [
      _vm._v(
        "\n            Lorem ipsum dolor sit amit,consectetur eiusmdd tempory\n            "
      ),
      _c("br"),
      _vm._v("incididunt ut labore et dolore magna elit\n          ")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass:
          "d-flex flex-wrap justify-content-start align-items-start mb-3"
      },
      [
        _c(
          "a",
          {
            staticClass: "text-blue mr-3",
            attrs: { href: "javascript: void(0);" }
          },
          [
            _c("i", { staticClass: "fe fe-heart mr-1" }),
            _vm._v(" 61 Likes\n            ")
          ]
        ),
        _vm._v(" "),
        _c(
          "a",
          {
            staticClass: "text-blue mr-3",
            attrs: { href: "javascript: void(0);" }
          },
          [
            _c("i", { staticClass: "fe fe-message-square mr-1" }),
            _vm._v(" 2 Comments\n            ")
          ]
        )
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/@kit/widgets/General/15/index.vue":
/*!********************************************************!*\
  !*** ./resources/js/@kit/widgets/General/15/index.vue ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _index_vue_vue_type_template_id_2300ae39___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.vue?vue&type=template&id=2300ae39& */ "./resources/js/@kit/widgets/General/15/index.vue?vue&type=template&id=2300ae39&");
/* harmony import */ var _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.vue?vue&type=script&lang=js& */ "./resources/js/@kit/widgets/General/15/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _index_vue_vue_type_template_id_2300ae39___WEBPACK_IMPORTED_MODULE_0__["render"],
  _index_vue_vue_type_template_id_2300ae39___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/@kit/widgets/General/15/index.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/@kit/widgets/General/15/index.vue?vue&type=script&lang=js&":
/*!*********************************************************************************!*\
  !*** ./resources/js/@kit/widgets/General/15/index.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/@kit/widgets/General/15/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/@kit/widgets/General/15/index.vue?vue&type=template&id=2300ae39&":
/*!***************************************************************************************!*\
  !*** ./resources/js/@kit/widgets/General/15/index.vue?vue&type=template&id=2300ae39& ***!
  \***************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_2300ae39___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=template&id=2300ae39& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/@kit/widgets/General/15/index.vue?vue&type=template&id=2300ae39&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_2300ae39___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_2300ae39___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);