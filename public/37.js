(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[37],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/@kit/widgets/Charts/6/index.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/@kit/widgets/Charts/6/index.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _data_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./data.json */ "./resources/js/@kit/widgets/Charts/6/data.json");
var _data_json__WEBPACK_IMPORTED_MODULE_0___namespace = /*#__PURE__*/__webpack_require__.t(/*! ./data.json */ "./resources/js/@kit/widgets/Charts/6/data.json", 1);
/* harmony import */ var v_chartist__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! v-chartist */ "./node_modules/v-chartist/dist/vue-chartist.min.js");
/* harmony import */ var v_chartist__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(v_chartist__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var chartist_plugin_tooltips_updated__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! chartist-plugin-tooltips-updated */ "./node_modules/chartist-plugin-tooltips-updated/dist/chartist-plugin-tooltip.js");
/* harmony import */ var chartist_plugin_tooltips_updated__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(chartist_plugin_tooltips_updated__WEBPACK_IMPORTED_MODULE_2__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'KitChart6',
  components: {
    'vue-chartist': v_chartist__WEBPACK_IMPORTED_MODULE_1___default.a
  },
  data: function data() {
    var options = {
      low: 0,
      chartPadding: {
        right: 0,
        left: 0,
        top: 5,
        bottom: 0
      },
      fullWidth: true,
      showPoint: true,
      lineSmooth: false,
      axisY: {
        showGrid: false,
        showLabel: false,
        offset: 0
      },
      axisX: {
        showGrid: false,
        showLabel: false,
        offset: 0
      },
      showArea: true,
      plugins: [chartist_plugin_tooltips_updated__WEBPACK_IMPORTED_MODULE_2___default()({
        anchorToPoint: false,
        appendToBody: true,
        seriesName: false
      })]
    };
    return {
      data: _data_json__WEBPACK_IMPORTED_MODULE_0__,
      options: options
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/crypto/index.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/crypto/index.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _data_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./data.json */ "./resources/js/views/dashboard/crypto/data.json");
var _data_json__WEBPACK_IMPORTED_MODULE_0___namespace = /*#__PURE__*/__webpack_require__.t(/*! ./data.json */ "./resources/js/views/dashboard/crypto/data.json", 1);
/* harmony import */ var _kit_widgets_Charts_1_index__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/@kit/widgets/Charts/1/index */ "./resources/js/@kit/widgets/Charts/1/index.vue");
/* harmony import */ var _kit_widgets_Charts_5_index__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/@kit/widgets/Charts/5/index */ "./resources/js/@kit/widgets/Charts/5/index.vue");
/* harmony import */ var _kit_widgets_Charts_6_index__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/@kit/widgets/Charts/6/index */ "./resources/js/@kit/widgets/Charts/6/index.vue");
/* harmony import */ var _kit_widgets_General_5_index__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/@kit/widgets/General/5/index */ "./resources/js/@kit/widgets/General/5/index.vue");
/* harmony import */ var _kit_widgets_General_7_index__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @/@kit/widgets/General/7/index */ "./resources/js/@kit/widgets/General/7/index.vue");
/* harmony import */ var _kit_widgets_General_12_index__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @/@kit/widgets/General/12/index */ "./resources/js/@kit/widgets/General/12/index.vue");
/* harmony import */ var _kit_widgets_Lists_3_index__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @/@kit/widgets/Lists/3/index */ "./resources/js/@kit/widgets/Lists/3/index.vue");
/* harmony import */ var _kit_widgets_Lists_6_index__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @/@kit/widgets/Lists/6/index */ "./resources/js/@kit/widgets/Lists/6/index.vue");
/* harmony import */ var _kit_widgets_Lists_15_index__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @/@kit/widgets/Lists/15/index */ "./resources/js/@kit/widgets/Lists/15/index.vue");
/* harmony import */ var v_chartist__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! v-chartist */ "./node_modules/v-chartist/dist/vue-chartist.min.js");
/* harmony import */ var v_chartist__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(v_chartist__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var chartist_plugin_tooltips_updated__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! chartist-plugin-tooltips-updated */ "./node_modules/chartist-plugin-tooltips-updated/dist/chartist-plugin-tooltip.js");
/* harmony import */ var chartist_plugin_tooltips_updated__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(chartist_plugin_tooltips_updated__WEBPACK_IMPORTED_MODULE_11__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//












/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    AirChart1: _kit_widgets_Charts_1_index__WEBPACK_IMPORTED_MODULE_1__["default"],
    AirChart5: _kit_widgets_Charts_5_index__WEBPACK_IMPORTED_MODULE_2__["default"],
    AirChart6: _kit_widgets_Charts_6_index__WEBPACK_IMPORTED_MODULE_3__["default"],
    AirGeneral5: _kit_widgets_General_5_index__WEBPACK_IMPORTED_MODULE_4__["default"],
    AirGeneral7: _kit_widgets_General_7_index__WEBPACK_IMPORTED_MODULE_5__["default"],
    AirGeneral12: _kit_widgets_General_12_index__WEBPACK_IMPORTED_MODULE_6__["default"],
    AirList3: _kit_widgets_Lists_3_index__WEBPACK_IMPORTED_MODULE_7__["default"],
    AirList6: _kit_widgets_Lists_6_index__WEBPACK_IMPORTED_MODULE_8__["default"],
    AirList15: _kit_widgets_Lists_15_index__WEBPACK_IMPORTED_MODULE_9__["default"],
    'vue-chartist': v_chartist__WEBPACK_IMPORTED_MODULE_10___default.a
  },
  data: function data() {
    return {
      data: _data_json__WEBPACK_IMPORTED_MODULE_0__,
      options: {
        fullWidth: !0,
        chartPadding: {
          right: 15,
          left: -15
        },
        low: 0,
        showArea: true,
        plugins: [chartist_plugin_tooltips_updated__WEBPACK_IMPORTED_MODULE_11___default()({
          anchorToPoint: false,
          appendToBody: true,
          seriesName: false
        })]
      }
    };
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/@kit/widgets/Charts/6/index.vue?vue&type=style&index=0&lang=scss&module=true&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-2!./node_modules/sass-loader/dist/cjs.js??ref--8-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/@kit/widgets/Charts/6/index.vue?vue&type=style&index=0&lang=scss&module=true& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".progressIcon {\n  width: 3.33rem;\n  height: 3.33rem;\n  text-align: center;\n  border-radius: 5px;\n  overflow: hidden;\n  flex-shrink: 0;\n}\n.progressIcon i {\n  vertical-align: middle;\n  line-height: 3.33rem;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/@kit/widgets/Charts/6/index.vue?vue&type=style&index=0&lang=scss&module=true&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-2!./node_modules/sass-loader/dist/cjs.js??ref--8-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/@kit/widgets/Charts/6/index.vue?vue&type=style&index=0&lang=scss&module=true& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/src??ref--8-2!../../../../../../node_modules/sass-loader/dist/cjs.js??ref--8-3!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=style&index=0&lang=scss&module=true& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/@kit/widgets/Charts/6/index.vue?vue&type=style&index=0&lang=scss&module=true&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/@kit/widgets/Charts/6/index.vue?vue&type=template&id=2afaf20a&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/@kit/widgets/Charts/6/index.vue?vue&type=template&id=2afaf20a& ***!
  \*******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("div", { staticClass: "card-body" }, [
        _c(
          "div",
          { staticClass: "text-dark font-size-18 font-weight-bold mb-1" },
          [_vm._v("Income Progress")]
        ),
        _vm._v(" "),
        _c("div", { staticClass: "text-gray-6 mb-2" }, [
          _vm._v("Revenue by location and date")
        ]),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "font-weight-bold font-size-36 text-dark mb-2" },
          [_vm._v("$390,012.01")]
        ),
        _vm._v(" "),
        _c("div", { staticClass: "d-flex align-items-center" }, [
          _c(
            "div",
            {
              staticClass: "bg-gray-4 text-white mr-3",
              class: _vm.$style.progressIcon
            },
            [_c("i", { staticClass: "fe fe-menu font-size-18" })]
          ),
          _vm._v(" "),
          _vm._m(0)
        ])
      ]),
      _vm._v(" "),
      _c("vue-chartist", {
        staticClass: "height-200 ct-hidden-points",
        class: _vm.$style.chart,
        attrs: { type: "Line", data: _vm.data, options: _vm.options }
      })
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "flex-grow-1" }, [
      _c(
        "div",
        {
          staticClass:
            "text-dark font-size-18 font-weight-bold text-nowrap mb-2"
        },
        [_vm._v("78% from $500,000.00")]
      ),
      _vm._v(" "),
      _c("div", { staticClass: "progress" }, [
        _c("div", {
          staticClass: "progress-bar bg-success",
          staticStyle: { width: "70%" }
        })
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/crypto/index.vue?vue&type=template&id=390a69de&":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/dashboard/crypto/index.vue?vue&type=template&id=390a69de& ***!
  \********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _vm._m(0),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-xl-8 col-lg-12" }, [
        _c(
          "div",
          { staticClass: "card" },
          [
            _c(
              "a-tabs",
              {
                staticClass: "air-tabs-bordered",
                attrs: { defaultActiveKey: "2" }
              },
              [
                _c("a-tab-pane", { key: "1", attrs: { tab: "Hour" } }),
                _vm._v(" "),
                _c("a-tab-pane", { key: "2", attrs: { tab: "Day" } }),
                _vm._v(" "),
                _c("a-tab-pane", { key: "3", attrs: { tab: "Week" } }),
                _vm._v(" "),
                _c("a-tab-pane", { key: "4", attrs: { tab: "Month" } })
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "card-body" },
              [
                _c("vue-chartist", {
                  staticClass: "height-300",
                  attrs: { type: "Line", data: _vm.data, options: _vm.options }
                })
              ],
              1
            )
          ],
          1
        ),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-lg-6" }, [
            _c("div", { staticClass: "card" }, [
              _c("div", { staticClass: "card-body" }, [_c("air-general-7")], 1)
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "card" }, [_c("air-chart-6")], 1),
            _vm._v(" "),
            _c("div", { staticClass: "card" }, [
              _c("div", { staticClass: "card-body" }, [_c("air-general-5")], 1)
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-lg-6" }, [
            _c("div", { staticClass: "card" }, [
              _c("div", { staticClass: "card-body" }, [_c("air-List-3")], 1)
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "card" }, [
              _c("div", { staticClass: "card-body" }, [_c("air-general-12")], 1)
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "card" }, [
              _c("div", { staticClass: "card-body" }, [_c("air-chart-5")], 1)
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-xl-4 col-lg-12" }, [
        _c("div", { staticClass: "card" }, [
          _vm._m(1),
          _vm._v(" "),
          _c("div", { staticClass: "card-body" }, [_c("air-List-6")], 1)
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "card" }, [
          _c("div", { staticClass: "card-body" }, [_c("air-List-15")], 1)
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "card" }, [
          _c("div", { staticClass: "card-body" }, [_c("air-chart-1")], 1)
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "air__utils__heading" }, [
      _c("h5", [_vm._v("Dashboard Crypto")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "card-header" }, [
      _c("h5", { staticClass: "text-dark mb-0" }, [_vm._v("Cryptocurrencies")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/@kit/widgets/Charts/6/data.json":
/*!******************************************************!*\
  !*** ./resources/js/@kit/widgets/Charts/6/data.json ***!
  \******************************************************/
/*! exports provided: series, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"series\":[{\"className\":\"ct-series-h\",\"data\":[4,6,4,3,5,7,8,7]}]}");

/***/ }),

/***/ "./resources/js/@kit/widgets/Charts/6/index.vue":
/*!******************************************************!*\
  !*** ./resources/js/@kit/widgets/Charts/6/index.vue ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var _index_vue_vue_type_template_id_2afaf20a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.vue?vue&type=template&id=2afaf20a& */ "./resources/js/@kit/widgets/Charts/6/index.vue?vue&type=template&id=2afaf20a&");
/* harmony import */ var _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.vue?vue&type=script&lang=js& */ "./resources/js/@kit/widgets/Charts/6/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _index_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./index.vue?vue&type=style&index=0&lang=scss&module=true& */ "./resources/js/@kit/widgets/Charts/6/index.vue?vue&type=style&index=0&lang=scss&module=true&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





var cssModules = {}
var disposed = false

function injectStyles (context) {
  if (disposed) return
  
        cssModules["$style"] = (_index_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_2__["default"].locals || _index_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_2__["default"])
        Object.defineProperty(this, "$style", {
          configurable: true,
          get: function () {
            return cssModules["$style"]
          }
        })
      
}


  module.hot && false



        module.hot && false

/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _index_vue_vue_type_template_id_2afaf20a___WEBPACK_IMPORTED_MODULE_0__["render"],
  _index_vue_vue_type_template_id_2afaf20a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  injectStyles,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/@kit/widgets/Charts/6/index.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./resources/js/@kit/widgets/Charts/6/index.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./resources/js/@kit/widgets/Charts/6/index.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/@kit/widgets/Charts/6/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/@kit/widgets/Charts/6/index.vue?vue&type=style&index=0&lang=scss&module=true&":
/*!****************************************************************************************************!*\
  !*** ./resources/js/@kit/widgets/Charts/6/index.vue?vue&type=style&index=0&lang=scss&module=true& ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/style-loader!../../../../../../node_modules/css-loader!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/src??ref--8-2!../../../../../../node_modules/sass-loader/dist/cjs.js??ref--8-3!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=style&index=0&lang=scss&module=true& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/@kit/widgets/Charts/6/index.vue?vue&type=style&index=0&lang=scss&module=true&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (default from non-harmony) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_0___default.a; });
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss_module_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 

/***/ }),

/***/ "./resources/js/@kit/widgets/Charts/6/index.vue?vue&type=template&id=2afaf20a&":
/*!*************************************************************************************!*\
  !*** ./resources/js/@kit/widgets/Charts/6/index.vue?vue&type=template&id=2afaf20a& ***!
  \*************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_2afaf20a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=template&id=2afaf20a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/@kit/widgets/Charts/6/index.vue?vue&type=template&id=2afaf20a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_2afaf20a___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_2afaf20a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/dashboard/crypto/data.json":
/*!*******************************************************!*\
  !*** ./resources/js/views/dashboard/crypto/data.json ***!
  \*******************************************************/
/*! exports provided: labels, series, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"labels\":[\"Sun\",\"Mon\",\"Tue\",\"Wed\",\"Thu\",\"Fri\",\"Sat\"],\"series\":[[2,4,7,25,5,28,31]]}");

/***/ }),

/***/ "./resources/js/views/dashboard/crypto/index.vue":
/*!*******************************************************!*\
  !*** ./resources/js/views/dashboard/crypto/index.vue ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _index_vue_vue_type_template_id_390a69de___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.vue?vue&type=template&id=390a69de& */ "./resources/js/views/dashboard/crypto/index.vue?vue&type=template&id=390a69de&");
/* harmony import */ var _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.vue?vue&type=script&lang=js& */ "./resources/js/views/dashboard/crypto/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _index_vue_vue_type_template_id_390a69de___WEBPACK_IMPORTED_MODULE_0__["render"],
  _index_vue_vue_type_template_id_390a69de___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/dashboard/crypto/index.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/dashboard/crypto/index.vue?vue&type=script&lang=js&":
/*!********************************************************************************!*\
  !*** ./resources/js/views/dashboard/crypto/index.vue?vue&type=script&lang=js& ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/crypto/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/dashboard/crypto/index.vue?vue&type=template&id=390a69de&":
/*!**************************************************************************************!*\
  !*** ./resources/js/views/dashboard/crypto/index.vue?vue&type=template&id=390a69de& ***!
  \**************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_390a69de___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=template&id=390a69de& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/dashboard/crypto/index.vue?vue&type=template&id=390a69de&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_390a69de___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_390a69de___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);