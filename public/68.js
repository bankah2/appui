(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[68],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/apps/wordpress-post/index.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/apps/wordpress-post/index.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _kit_widgets_General_15_index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/@kit/widgets/General/15/index */ "./resources/js/@kit/widgets/General/15/index.vue");
/* harmony import */ var _kit_widgets_Lists_15_index__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/@kit/widgets/Lists/15/index */ "./resources/js/@kit/widgets/Lists/15/index.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    KitGeneral15: _kit_widgets_General_15_index__WEBPACK_IMPORTED_MODULE_0__["default"],
    KitList15: _kit_widgets_Lists_15_index__WEBPACK_IMPORTED_MODULE_1__["default"]
  },
  data: function data() {
    return {
      form: this.$form.createForm(this)
    };
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/apps/wordpress-post/index.vue?vue&type=template&id=42d5eb05&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/apps/wordpress-post/index.vue?vue&type=template&id=42d5eb05& ***!
  \***********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-xl-9 col-lg-12" }, [
        _vm._m(0),
        _vm._v(" "),
        _c("div", { staticClass: "card" }, [
          _c(
            "div",
            { staticClass: "card-body" },
            [
              _vm._m(1),
              _vm._v(" "),
              _c("kit-general-15"),
              _vm._v(" "),
              _c(
                "a",
                {
                  staticClass: "d-block btn btn-light text-primary mt-3",
                  attrs: { href: "javascript: void(0);" }
                },
                [_vm._v("Load More")]
              )
            ],
            1
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "card" }, [
          _c(
            "div",
            { staticClass: "card-body" },
            [
              _vm._m(2),
              _vm._v(" "),
              _c("h5", { staticClass: "text-dark mb-4" }, [
                _vm._v("Leave a comment")
              ]),
              _vm._v(" "),
              _c(
                "a-form",
                { attrs: { form: _vm.form } },
                [
                  _c(
                    "a-form-item",
                    [
                      _c(
                        "a-input",
                        {
                          directives: [
                            {
                              name: "decorator",
                              rawName: "v-decorator",
                              value: [
                                "username",
                                {
                                  rules: [
                                    {
                                      required: true,
                                      message: "Please input your username!"
                                    }
                                  ]
                                }
                              ],
                              expression:
                                "['username', {rules: [{ required: true, message: 'Please input your username!' }]}]"
                            }
                          ],
                          attrs: { placeholder: "Username" }
                        },
                        [
                          _c("a-icon", {
                            staticStyle: { color: "rgba(0,0,0,.25)" },
                            attrs: { slot: "prefix", type: "user" },
                            slot: "prefix"
                          })
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "a-form-item",
                    [
                      _c(
                        "a-input",
                        {
                          directives: [
                            {
                              name: "decorator",
                              rawName: "v-decorator",
                              value: ["email"],
                              expression: "['email']"
                            }
                          ],
                          attrs: { placeholder: "Email" }
                        },
                        [
                          _c("a-icon", {
                            staticStyle: { color: "rgba(0,0,0,.25)" },
                            attrs: { slot: "prefix", type: "mail" },
                            slot: "prefix"
                          })
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "a-form-item",
                    [
                      _c("a-textarea", {
                        attrs: { rows: 3, placeholder: "Your message" }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "a-form-item",
                    [
                      _c(
                        "a-button",
                        {
                          staticClass: "mr-2",
                          staticStyle: { width: "200px" },
                          attrs: { type: "primary" }
                        },
                        [
                          _c("i", { staticClass: "fa fa-send mr-2" }),
                          _vm._v(" Send\n              ")
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "a-upload",
                        [
                          _c(
                            "a-button",
                            [
                              _c("a-icon", { attrs: { type: "upload" } }),
                              _vm._v("Attach File\n                ")
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-xl-3 col-lg-12" }, [
        _vm._m(3),
        _vm._v(" "),
        _vm._m(4),
        _vm._v(" "),
        _vm._m(5),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "pb-4 mb-3 border-bottom" },
          [
            _c("div", { staticClass: "font-weight-bold mb-3" }, [
              _vm._v("Latest Posts")
            ]),
            _vm._v(" "),
            _c("kit-list-15")
          ],
          1
        )
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "card" }, [
      _c("div", { staticClass: "card-body" }, [
        _c("div", { staticClass: "mb-2" }, [
          _c(
            "a",
            {
              staticClass: "text-dark font-size-24 font-weight-bold",
              attrs: { href: "javascript: void(0);" }
            },
            [
              _vm._v(
                "[Feature Request] How to enable custom font that comes from svg #2460"
              )
            ]
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "mb-3" }, [
          _c(
            "a",
            {
              staticClass: "font-weight-bold",
              attrs: { href: "javascript: void(0);" }
            },
            [_vm._v("zxs2162")]
          ),
          _vm._v(
            " wrote this post 12\n            days ago · 0 comments\n          "
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "mb-4" }, [
          _c(
            "a",
            {
              staticClass:
                "badge text-blue text-uppercase bg-light font-size-12 mr-2",
              attrs: { href: "javascript: void(0);" }
            },
            [_vm._v("Umi")]
          ),
          _vm._v(" "),
          _c(
            "a",
            {
              staticClass:
                "badge text-blue text-uppercase bg-light font-size-12 mr-2",
              attrs: { href: "javascript: void(0);" }
            },
            [_vm._v("React-framework")]
          ),
          _vm._v(" "),
          _c(
            "a",
            {
              staticClass:
                "badge text-blue text-uppercase bg-light font-size-12 mr-2",
              attrs: { href: "javascript: void(0);" }
            },
            [_vm._v("Umijs")]
          )
        ]),
        _vm._v(" "),
        _c("div", [
          _c("img", {
            staticClass: "img-fluid mb-4",
            attrs: { src: "resources/images/content/photos/1.jpeg", alt: "Sea" }
          }),
          _vm._v(" "),
          _c("p", [
            _vm._v(
              "\n              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil laborum est perferendis\n              consectetur corporis esse labore minima molestias, exercitationem consequuntur! Lorem\n              ipsum dolor sit amet, consectetur adipisicing elit. Nihil laborum est perferendis\n              consectetur corporis esse labore minima molestias, exercitationem consequuntur! Lorem\n              ipsum dolor sit amet, consectetur adipisicing elit. Nihil laborum est perferendis\n              consectetur corporis esse labore minima molestias, exercitationem consequuntur! Lorem\n              ipsum dolor sit amet, consectetur adipisicing elit. Nihil laborum est perferendis\n              consectetur corporis esse labore minima molestias, exercitationem consequuntur!\n            "
            )
          ]),
          _vm._v(" "),
          _c("p", [
            _vm._v(
              "\n              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil laborum est perferendis\n              consectetur corporis esse labore minima molestias, exercitationem consequuntur! Lorem\n              ipsum dolor sit amet, consectetur adipisicing elit. Nihil laborum est perferendis\n              consectetur corporis esse labore minima molestias, exercitationem consequuntur!\n            "
            )
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h6", { staticClass: "mb-4 text-uppercase" }, [
      _c("strong", [_vm._v("Comments (76)")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass:
          "d-flex align-items-center flex-wrap border-bottom mb-3 pb-3"
      },
      [
        _c(
          "div",
          {
            staticClass:
              "kit__utils__avatar kit__utils__avatar--size110 mr-3 mb-3 align-items-center flex-shrink-0"
          },
          [
            _c("img", {
              attrs: {
                src: "resources/images/avatars/5.jpg",
                alt: "Mary Stanform"
              }
            })
          ]
        ),
        _vm._v(" "),
        _c("div", { staticClass: "mb-3" }, [
          _c(
            "div",
            { staticClass: "font-weight-bold font-size-16 text-dark mb-2" },
            [_vm._v("Trinity Parson")]
          ),
          _vm._v(" "),
          _c("p", { staticClass: "font-italic" }, [
            _vm._v(
              "“I hope you enjoy reading this as much as I enjoyed writing this.”"
            )
          ]),
          _vm._v(" "),
          _c(
            "a",
            { staticClass: "btn btn-sm btn-primary", attrs: { href: "#" } },
            [_vm._v("View Profile")]
          )
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "pb-4 mb-3 border-bottom" }, [
      _c(
        "label",
        {
          staticClass: "font-weight-bold d-block",
          attrs: { for: "search-input" }
        },
        [
          _c("span", { staticClass: "mb-2 d-inline-block" }, [
            _vm._v("Search Post")
          ]),
          _vm._v(" "),
          _c("input", {
            staticClass: "form-control width-100p",
            attrs: {
              type: "text",
              placeholder: "Search post...",
              id: "search-input"
            }
          })
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "pb-4 mb-3 border-bottom" }, [
      _c(
        "label",
        {
          staticClass: "font-weight-bold d-block",
          attrs: { for: "subscribe-input" }
        },
        [
          _c("span", { staticClass: "mb-2 d-inline-block" }, [
            _vm._v("Subscribe")
          ]),
          _vm._v(" "),
          _c("input", {
            staticClass: "form-control width-100p",
            attrs: {
              type: "text",
              id: "subscribe-input",
              placeholder: "Enter your email..."
            }
          })
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "pb-4 mb-3 border-bottom" }, [
      _c("div", { staticClass: "font-weight-bold mb-2" }, [
        _vm._v("Categories")
      ]),
      _vm._v(" "),
      _c("div", [
        _c(
          "a",
          {
            staticClass:
              "badge text-blue text-uppercase bg-light font-size-12 mr-2",
            attrs: { href: "javascript: void(0);" }
          },
          [_vm._v("Umi")]
        ),
        _vm._v(" "),
        _c(
          "a",
          {
            staticClass:
              "badge text-blue text-uppercase bg-light font-size-12 mr-2",
            attrs: { href: "javascript: void(0);" }
          },
          [_vm._v("React-framework")]
        ),
        _vm._v(" "),
        _c(
          "a",
          {
            staticClass:
              "badge text-blue text-uppercase bg-light font-size-12 mr-2",
            attrs: { href: "javascript: void(0);" }
          },
          [_vm._v("Umijs")]
        )
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/apps/wordpress-post/index.vue":
/*!**********************************************************!*\
  !*** ./resources/js/views/apps/wordpress-post/index.vue ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _index_vue_vue_type_template_id_42d5eb05___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.vue?vue&type=template&id=42d5eb05& */ "./resources/js/views/apps/wordpress-post/index.vue?vue&type=template&id=42d5eb05&");
/* harmony import */ var _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.vue?vue&type=script&lang=js& */ "./resources/js/views/apps/wordpress-post/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _index_vue_vue_type_template_id_42d5eb05___WEBPACK_IMPORTED_MODULE_0__["render"],
  _index_vue_vue_type_template_id_42d5eb05___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/apps/wordpress-post/index.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/apps/wordpress-post/index.vue?vue&type=script&lang=js&":
/*!***********************************************************************************!*\
  !*** ./resources/js/views/apps/wordpress-post/index.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/apps/wordpress-post/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/apps/wordpress-post/index.vue?vue&type=template&id=42d5eb05&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/views/apps/wordpress-post/index.vue?vue&type=template&id=42d5eb05& ***!
  \*****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_42d5eb05___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=template&id=42d5eb05& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/apps/wordpress-post/index.vue?vue&type=template&id=42d5eb05&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_42d5eb05___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_42d5eb05___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);